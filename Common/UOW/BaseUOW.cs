﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.UOW
{
    public class BaseUOW : IDisposable
    {
        #region Public Methods
        public virtual void Dispose()
        {
        }
        #endregion

    }
}
