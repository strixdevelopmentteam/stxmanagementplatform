﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Logs
{
    public class ErrorLogger
    {
        public static void LogException(EventLogEntryType ErrorType, string sClass, string sFunction, string sMessage, string stackTrace)
        {
            string logError = sClass + ", function:  " + sFunction + " message," + sMessage + "\n" + stackTrace;
            try
            {
                if (!EventLog.SourceExists("ManagementPlatform.EmpresaTerceiros"))
                    EventLog.CreateEventSource("ManagementPlatform.EmpresaTerceiros", "GestratoNet");

                using (EventLog logEntry = new EventLog("Libs", ".", "ManagementPlatform"))
                {
                    lock (Locks.Locks.Instance.LogErrorLock)
                    {
                        logEntry.WriteEntry(logError, ErrorType);
                    }
                }
            }
            catch (Exception ex)
            {
                LogToFile(ErrorType, sClass, sFunction, sMessage, stackTrace);
            }
        }

        public static void LogToFile(EventLogEntryType ErrorType, string sClass, string sFunction, string sMessage, string stackTrace)
        {
            string logError = sClass + ", function:  " + sFunction + " message," + sMessage + "\n" + stackTrace;
            try
            {
                lock (Locks.Locks.Instance.LogErrorLock)
                {
                    File.AppendAllText("Log.txt", logError);
                }
            }
            catch (Exception ex)
            {
                lock (Locks.Locks.Instance.LogErrorLock)
                {
                    File.AppendAllText("Error logging " + ex.Message, "ErrorLogging.txt");
                }
            }
        }

    }
}
