﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Locks
{
    public class Locks
    {
        #region Fields
        private readonly object logErrorLock = new object();
        private readonly object dbLock = new object();
        private static Locks instance;
        #endregion

        #region Properties
        public static Locks Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Locks();
                }
                return instance;
            }
        }
        public object LogErrorLock
        {
            get
            {
                return logErrorLock;
            }
        }
        public object DbLock
        {
            get
            {
                return dbLock;
            }
        }
        
        #endregion
    }
}
