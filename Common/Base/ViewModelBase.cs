﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Common.Base
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            this.PropertyChangedCompleted(propertyName);
        }
        protected virtual void PropertyChangedCompleted(string propertyName)
        {
        }
        #endregion
    }
}
