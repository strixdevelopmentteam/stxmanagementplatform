﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedboothDatabase
{
    public class RedboothDBContext : DbContext
    {
        static RedboothDBContext()
        {
            SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<RedboothDBContext, Migrations.Configuration>());
        }

        public RedboothDBContext() :
        //base("Data Source=LTAROUCO;Initial Catalog=RedboothDB;User ID=sa;Password=StrixAdmin;Connect Timeout=15;Type System Version=SQL Server 2012;")
        base(ConfigurationManager.ConnectionStrings["RedboothDBConnectionString"].ConnectionString)
        {

        }

        public DbSet<RedboothData> RedboothData { get; set; }
        public DbSet<ImportMetadata> ImportMetadata { get; set; }

    }
}
