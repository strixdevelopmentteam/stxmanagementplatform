﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RedboothDatabase
{
    public class RedboothData
    {
        [Key]
        public int Id { get; set; }

        public DateTime? Date { get; set; }
        public string Organization { get; set; }
        public string Project { get; set; }
        public string Task { get; set; }
        public string TaskList { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public double Time_Hours { get; set; }
        public TimeSpan? Durationhhmmss { get; set; }
        public TimeSpan? Durationddhhmm { get; set; }
        public string Comment { get; set; }
        public string Assignee { get; set; }
        public string Status { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? Completed { get; set; }
        public string Tags { get; set; }
        public ImportMetadata ImportMetaData { get; set; }
    }
}
