﻿namespace RedboothDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RedboothDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Organization = c.String(),
                        Project = c.String(),
                        Task = c.String(),
                        TaskList = c.String(),
                        Name = c.String(),
                        UserName = c.String(),
                        Time_Hours = c.Double(nullable: false),
                        Durationhhmmss = c.Time(nullable: false, precision: 7),
                        Durationddhhmm = c.Time(nullable: false, precision: 7),
                        Comment = c.String(),
                        Assignee = c.String(),
                        Status = c.String(),
                        DueDate = c.Double(nullable: false),
                        Completed = c.DateTime(nullable: false),
                        Tags = c.String(),
                        ImportMetaDataId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RedboothDatas");
        }
    }
}
