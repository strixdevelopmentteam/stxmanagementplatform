﻿namespace RedboothDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableDateTimes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RedboothDatas", "Date", c => c.DateTime());
            AlterColumn("dbo.RedboothDatas", "DueDate", c => c.DateTime());
            AlterColumn("dbo.RedboothDatas", "Completed", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RedboothDatas", "Completed", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RedboothDatas", "DueDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RedboothDatas", "Date", c => c.DateTime(nullable: false));
        }
    }
}
