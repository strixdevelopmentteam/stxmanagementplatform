﻿namespace RedboothDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RetypeDueDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RedboothDatas", "DueDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RedboothDatas", "DueDate", c => c.Double(nullable: false));
        }
    }
}
