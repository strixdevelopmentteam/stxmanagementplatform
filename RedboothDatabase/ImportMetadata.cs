﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RedboothDatabase
{
    public class ImportMetadata
    {
        [Key]
        public int Id { get; set; }

        public DateTime TimeStamp { get; set; }
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public string ImportedBy { get; set; }
    }
}
