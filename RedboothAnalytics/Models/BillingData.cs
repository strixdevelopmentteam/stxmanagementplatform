﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analytics.Models
{
    public class BillingData
    {
        public DateTime Date { get; set; }
        public double Prediction { get; set; }
        public double Billed { get; set; }
        public double Baseline { get; set; }
        public string Sheet { get; set; }
    }
}
