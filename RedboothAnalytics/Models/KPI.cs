﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analytics.Models
{
    public class KPI
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string ValueJan { get; set; }
        public string ValueFev { get; set; }
        public string ValueMar { get; set; }
        public string ValueAbr { get; set; }
        public string ValueMai { get; set; }
        public string ValueJun { get; set; }
        public string ValueJul { get; set; }
        public string ValueAgo { get; set; }
        public string ValueSet { get; set; }
        public string ValueOut { get; set; }
        public string ValueNov { get; set; }
        public string ValueDez { get; set; }

        public string Units { get; set; }
        public string Department { get; set; }
        public double Target { get; set; }
        public string Evaluation { get; set; }
    }
}
