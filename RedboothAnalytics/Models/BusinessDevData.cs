﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analytics.Models
{
    public class BusinessDevData
    {
        public string Proposal { get; set; }
        public string Client { get; set; }
        public double Value { get; set; }
        public double Overhead { get; set; }
        public double Margin { get; set; }
        public DateTime DateSent { get; set; }
        public bool Adjudicated { get; set; }
        public DateTime DateAdjudicated { get; set; }
        public double ManDay { get; set; }
        public string Area { get; set; }
        public bool NewClient { get; set; }
    }
}
