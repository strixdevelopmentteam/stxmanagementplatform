﻿using RedboothDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analytics.Models
{
    public class TimeReport
    {
        public String Name { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public double WorkedHours { get; set; }
        public double RestHours { get; set; }
        public double LeaveHours { get; set; }
        public double TotalavailableHours { get; set; }
        public List<RedboothData> TaskList { get; set; }
}
}
