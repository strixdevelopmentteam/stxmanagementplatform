﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analytics.People
{

    public enum STXDepartment
    {
        BioDepartmentInterns, BioDepartmentExterns, ITDepartment, BusinessDevDepartment, Management
    }

    public class UserID
    {
        public string Name;
        public string Email;
        public STXDepartment Department;
        public UserID(string name, string email, STXDepartment department)
        {
            Name = name;
            Email = email;
            Department = department;
        }
    }

    public static class StrixTeam
    {

        public struct Management
        {
            public static UserID RicardoOliveira = new UserID("Ricardo Oliveira", "ricardo.oliveira@strix.pt", STXDepartment.Management);
            public static UserID MiguelRepas = new UserID("Miguel Repas", "miguel.repas@strix.pt", STXDepartment.Management);
            public static UserID EdgarSecca = new UserID("Edgar Secca", "edgar.secca@strix.pt", STXDepartment.Management);
        }

        public struct TecnologicTeam
        {
            public static UserID RicardoOliveira = new UserID("Ricardo Oliveira", "ricardo.oliveira@strix.pt", STXDepartment.ITDepartment);
            public static UserID NunoAlmeida = new UserID("Nuno Almeida", "nuno.almeida@strix.pt", STXDepartment.ITDepartment);
            public static UserID NadinePires = new UserID("Nadine Pires", "nadine.pires@strix.pt", STXDepartment.ITDepartment);
        }
        public struct BiodiversityTeamInterns
        {
            public static UserID NadinePires = new UserID("Nadine Pires", "nadine.pires@strix.pt", STXDepartment.BioDepartmentInterns);
            public static UserID FilipeCanario = new UserID("Filipe Canário", "filipe.canario@strix.pt", STXDepartment.BioDepartmentInterns);
            public static UserID AlexandreLeitao = new UserID("Alexandre Hespanhol Leitão", "alexandre.leitao@strix.pt", STXDepartment.BioDepartmentInterns);
            public static UserID NunoVieira = new UserID("Nuno Vieira", "nuno.vieira@strix.pt", STXDepartment.BioDepartmentInterns);
            public static UserID PedroMoreira = new UserID("Pedro Moreira", "pedro.moreira@strix.pt", STXDepartment.BioDepartmentInterns);
            public static UserID LuisSilva = new UserID("Luís Silva", "luis.silva@strix.pt", STXDepartment.BioDepartmentInterns);
            public static UserID RicardoTome = new UserID("Ricardo Tomé", "ricardo.tome@strix.pt", STXDepartment.BioDepartmentInterns);
            public static UserID MartaSampaio = new UserID("Marta Sampaio", "marta.sampaio@strix.pt", STXDepartment.BioDepartmentInterns);
        }

        public struct BiodiversityTeamExterns
        {
            public static UserID FredericoMartins = new UserID("Frederico Martins", "frederico.martins@strix.pt", STXDepartment.BioDepartmentExterns);
            public static UserID JoanaFernandes = new UserID("Joana Fernandes", "joana.fernandes@strix.pt", STXDepartment.BioDepartmentExterns);
        }

        public struct BusinessDevTeam
        {
            public static UserID AnaHorta = new UserID("Ana Horta", "ana.horta@strix.pt", STXDepartment.BusinessDevDepartment);
            public static UserID MartaSampaio = new UserID("Marta Sampaio", "marta.sampaio@strix.pt", STXDepartment.BusinessDevDepartment);
        }
        public static List<UserID> GetItPeople(DateTime dt)
        {
            List<UserID> its = new List<UserID>();

            if ( (dt >= new DateTime(2019, 3, 1)) && (dt <= new DateTime(2020, 08, 31)) )
            {
                its.Add(TecnologicTeam.NunoAlmeida);
            }
            if (dt < new DateTime(2019, 12, 01))
            {
                its.Add(TecnologicTeam.NadinePires);
            }
            its.Add(TecnologicTeam.RicardoOliveira);

            return its;
        }

        public static List<UserID> GetManagementPeople(DateTime dt)
        {
            List<UserID> its = new List<UserID>();
            its.Add(Management.MiguelRepas);
            its.Add(Management.RicardoOliveira);
            its.Add(Management.EdgarSecca);
            return its;
        }


        public static List<UserID> GetBiodiversityManagers(DateTime dt)
        {
            List<UserID> biodiversity = new List<UserID>();
            biodiversity.Add(BiodiversityTeamInterns.FilipeCanario);
            if (dt > new DateTime(2020, 01, 01))
            {
                biodiversity.Add(BiodiversityTeamInterns.RicardoTome);
               
            }
            return biodiversity;
        }

        public static List<UserID> GetBusinessDevManagers(DateTime dt)
        {
            List<UserID> businessDev = new List<UserID>();
            if (dt > new DateTime(2019, 01, 01))
            {
                businessDev.Add(BusinessDevTeam.AnaHorta);
            }
            return businessDev;
        }

        public static List<UserID> GetTechDevManagers(DateTime dt)
        {
            List<UserID> itManagers = new List<UserID>();
            itManagers.Add(TecnologicTeam.RicardoOliveira);
            return itManagers;
        }
        public static List<UserID> GetBiodiversityExterns(DateTime dt)
        {
            List<UserID> biodiversity = new List<UserID>();
            biodiversity.Add(BiodiversityTeamExterns.FredericoMartins);
            biodiversity.Add(BiodiversityTeamExterns.JoanaFernandes);
            return biodiversity;
        }

        public static List<UserID> GetBiodiversityInterns(DateTime dt)
        {
            List<UserID> biodiversity = new List<UserID>();
            biodiversity.Add(BiodiversityTeamInterns.RicardoTome);
            biodiversity.Add(BiodiversityTeamInterns.AlexandreLeitao);
            biodiversity.Add(BiodiversityTeamInterns.FilipeCanario);
            
            biodiversity.Add(BiodiversityTeamInterns.LuisSilva);
            biodiversity.Add(BiodiversityTeamInterns.NadinePires);
            biodiversity.Add(BiodiversityTeamInterns.NunoVieira);
            biodiversity.Add(BiodiversityTeamInterns.PedroMoreira);

            if (dt < new DateTime(2019,12,01))
            {
                biodiversity.Add(BiodiversityTeamInterns.MartaSampaio);
            }

            return biodiversity;
        }

        public static List<UserID> GetBusinessDevPeople(DateTime dt)
        {
            List<UserID> businessDevs = new List<UserID>();

            businessDevs.Add(BusinessDevTeam.AnaHorta);

            if ( (dt >= new DateTime(2019, 12, 01)) && (dt >= new DateTime(2020, 10, 30)) )
            {
                businessDevs.Add(BusinessDevTeam.MartaSampaio);
            }

            return businessDevs;
        }
    }

    public static class DepartmentKPIs
    {
        public struct Biodiversity
        {
            public static Tuple<string, string> RedboothPercentageReportedHours = new Tuple<string, string>("RedBooth reported hours in percentage", "%");
            public static Tuple<string,string> ExecutingProjectsHours = new Tuple<string, string>("Hours Executing projects", "h");
            public static Tuple<string, string> PercentageHoursNotExecutingProjets = new Tuple<string, string>("% Hours not in projects","%");
            public static Tuple<string, string> PercentageHoursLeave = new Tuple<string, string>("% Hours Leave Time", "%");

            public static Tuple<string, string> ManDayExecutingProjects = new Tuple<string, string>("Man Day Executing projects", "Man*Day");
            public static Tuple<string, string> ManDayNotExecutingProjets = new Tuple<string, string>("Man Day Not in projects", "Man*Day");
            public static Tuple<string, string> ManDayLeave = new Tuple<string, string>("Man Day Leave", "Man*Day");

        }
        public struct IT
        {
            public static Tuple<string, string> RedboothPercentageReportedHours = new Tuple<string, string>("RedBooth reported hours in percentage", "%");
            public static Tuple<string, string> HoursNotSoftware = new Tuple<string, string>("Hours No Software", "%");
            public static Tuple<string, string> PercentageHoursNotSoftware = new Tuple<string, string>("Percentage hours not software", "%");
            public static Tuple<string, string> PercentageHoursLeave = new Tuple<string, string>("% Hours Leave Time", "%");
        }

        public struct BusinessDevelopment
        {
            // from redbooth
            public static Tuple<string, string> RedboothPercentageReportedHours = new Tuple<string, string>("RedBooth reported hours in percentage", "%");
            public static Tuple<string, string> PercentageHoursLeave = new Tuple<string, string>("% Hours Leave Time", "%");
            // Cumulatives from Proposals XLS
            public static Tuple<string, string> TotalNumberOfProposals = new Tuple<string, string>("Cumulative: Issued Proposals", "#");
            public static Tuple<string, string> TotalNumberOfAdjudicatedProposals = new Tuple<string, string>("Cumulative: Adjudicated Proposals", "#");
            public static Tuple<string, string> TotalValueOfProposals = new Tuple<string, string>("Cumulative: Value of Issued Proposals", "€");
            public static Tuple<string, string> TotalValueOfAdjudicatedProposals = new Tuple<string, string>("Cumulative: Value of approved Proposals", "€");
            public static Tuple<string, string> RatioOfAdjudicatedValueProposals = new Tuple<string, string>("Cumulative: Ratio Approved/Issued value", "%");
            public static Tuple<string, string> RatioOfAdjudicatedProposals = new Tuple<string, string>("Cumulative: Ratio Approved/Issued", "%");
            public static Tuple<string, string> NetProfit = new Tuple<string, string>("Quarterly: Net Profit", "%");

            public static Tuple<string, string> RacioOfProposalsBirdtrack = new Tuple<string, string>("Cumulative: Approved/Issued racio of \"Birdtrack\" Proposals", "%");
            public static Tuple<string, string> RacioOfProposalsEiaStudies = new Tuple<string, string>("Cumulative: Approved/Issued racio \"EIA/Studies\" Proposals", "%");
            public static Tuple<string, string> RacioOfProposalsEcologie = new Tuple<string, string>("Cumulative: Approved/Issued racio \"Ecology\" Proposals", "%");
            public static Tuple<string, string> RacioOfProposalsOthers = new Tuple<string, string>("Cumulative: Approved/Issued racio \"Other\" Proposals", "%");
            public static Tuple<string, string> RacioOfProposalsTotal = new Tuple<string, string>("Cumulative: Approved/Issued racio \"Total\" Proposals", "%");
            public static Tuple<string, string> ManDayAdjudicated = new Tuple<string, string>("Cumulative: Work days adjudicated", "#");
            public static Tuple<string, string> AvgMarginBirdtrack = new Tuple<string, string>("Cumulative:  Avg margin \"Birdtrack\"", "%");
            public static Tuple<string, string> AvgMarginEIA = new Tuple<string, string>("Cumulative: Avg margin \"EIA/Studies\"", "%");
            public static Tuple<string, string> AvgMarginEcologie = new Tuple<string, string>("Cumulative: Avg margin \"Ecology\"", "%");
            public static Tuple<string, string> AvgMarginOthers = new Tuple<string, string>("Cumulative: Avg margin \"Other\"", "%");
            public static Tuple<string, string> AvgMarginTotal = new Tuple<string, string>("Cumulative: Avg margin \"Total\"", "%");
            public static Tuple<string, string> RacioOfExistingClient = new Tuple<string, string>("Cumulative: Racio of Existing Customer", "%");
            public static Tuple<string, string> NumberNewCustomers = new Tuple<string, string>("Cumulative: New Customers count", "#");
            // Monthly
            public static Tuple<string, string> MonthlyTotalNumberOfProposals = new Tuple<string, string>("Monthly: Issued Proposals", "#");
            public static Tuple<string, string> MonthlyTotalValueOfProposals = new Tuple<string, string>("Monthly: Value of Issued Proposals", "€");
            public static Tuple<string, string> MonthlyTotalValueOfAdjudicatedProposals = new Tuple<string, string>("Monthly: Value of approved Proposals", "€");
            public static Tuple<string, string> MonthlyRatioOfAdjudicatedValueProposals = new Tuple<string, string>("Monthly: Ratio Approved/Issued value", "%");
            public static Tuple<string, string> MonthlyRacioOfProposalsBirdtrack = new Tuple<string, string>("Monthly: Approved/Issued racio of \"Birdtrack\" Proposals", "%");
            public static Tuple<string, string> MonthlyRacioOfProposalsEiaStudies = new Tuple<string, string>("Monthly: Approved/Issued racio \"EIA/Studies\" Proposals", "%");
            public static Tuple<string, string> MonthlyRacioOfProposalsEcologie = new Tuple<string, string>("Monthly: Approved/Issued racio \"Ecology\" Proposals", "%");
            public static Tuple<string, string> MonthlyRacioOfProposalsOthers = new Tuple<string, string>("Monthly: Approved/Issued racio \"Other\" Proposals", "%");
            public static Tuple<string, string> MonthlyRacioOfProposalsTotal = new Tuple<string, string>("Monthly: Approved/Issued racio \"Total\" Proposals", "%");
            public static Tuple<string, string> MonthlyManDayAdjudicated = new Tuple<string, string>("Monthly: Work days adjudicated", "#");
            public static Tuple<string, string> MonthlyAvgMarginBirdtrack = new Tuple<string, string>("Monthly:  Avg margin \"Birdtrack\"", "%");
            public static Tuple<string, string> MonthlyAvgMarginEIA = new Tuple<string, string>("Monthly: Avg margin \"EIA/Studies\"", "%");
            public static Tuple<string, string> MonthlyAvgMarginEcologie = new Tuple<string, string>("Monthly: Avg margin \"Ecology\"", "%");
            public static Tuple<string, string> MonthlyAvgMarginOthers = new Tuple<string, string>("Monthly: Avg margin \"Other\"", "%");
            public static Tuple<string, string> MonthlyAvgMarginTotal = new Tuple<string, string>("Monthly: Avg margin \"Total\"", "%");
            public static Tuple<string, string> MonthlyRacioOfExistingClient = new Tuple<string, string>("Monthly: Racio of Existing Customer", "%");
            public static Tuple<string, string> MonthlyNumberNewCustomers = new Tuple<string, string>("Monthly: New Customers count", "#");
        }
    }

    public static class Departments
    {
        public static string BiodiversityInterns = "Biodiversity Interns";
        public static string BiodiversityExterns = "Biodiversity Externs";
        public static string IT = "IT";
        public static string BusinessDevelopment = "BusinessDevelopment";
    }
}

