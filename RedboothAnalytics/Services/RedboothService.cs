﻿using Common.Logs;
using OfficeOpenXml;
using Analytics.Models;
using Analytics.People;
using RedboothDatabase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;

namespace Analytics.Services
{
    public class RedboothService
    {
        #region Fields
        List<DateTime> refBankHolidays = new List<DateTime>();
        List<RedboothData> listRedboothData = new List<RedboothData>();
        #endregion

        #region Constructor
        public RedboothService()
        {
            refBankHolidays.Add(new DateTime(1900, 1, 1));
            refBankHolidays.Add(new DateTime(1900, 4, 25));
            refBankHolidays.Add(new DateTime(1900, 5, 1));
            refBankHolidays.Add(new DateTime(1900, 6, 10));
            refBankHolidays.Add(new DateTime(1900, 1, 1));
            refBankHolidays.Add(new DateTime(1900, 1, 1));
            refBankHolidays.Add(new DateTime(1900, 1, 1));
            refBankHolidays.Add(new DateTime(1900, 1, 1));
            refBankHolidays.Add(new DateTime(1900, 1, 1));
            refBankHolidays.Add(new DateTime(1900, 1, 1));
        }
        #endregion

        #region Public methods
        public async Task<bool> ImportExcelToDataBase(string filePath, bool reset = false)
        {
            bool ret = false;
            await Task.Run(() =>
            {
                if (reset)
                {
                    listRedboothData = new List<RedboothData>();
                }
                string json = JsonConvert.SerializeObject(listRedboothData, Formatting.Indented);
                int cnt = 2;
                try
                {
                    ImportMetadata metadata = new ImportMetadata();

                    var package = new ExcelPackage(new FileInfo(filePath));
                    ExcelWorksheet sheet = package.Workbook.Worksheets[1];

                    while (true)
                    {
                        RedboothData redboothData = new RedboothData();

                        if (sheet.Cells[cnt, 1].Text.Length == 0)
                        {
                            break;
                        }

                        redboothData.Date = DateTime.ParseExact(sheet.Cells[cnt, 1].Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                        redboothData.Organization = sheet.Cells[cnt, 2].Text;
                        redboothData.Project = sheet.Cells[cnt, 3].Text;
                        redboothData.Task = sheet.Cells[cnt, 4].Text;
                        redboothData.TaskList = sheet.Cells[cnt, 5].Text;
                        redboothData.Name = sheet.Cells[cnt, 6].Text;
                        redboothData.UserName = sheet.Cells[cnt, 7].Text;
                        redboothData.Time_Hours = Double.Parse(sheet.Cells[cnt, 8].Text);
                        redboothData.Comment = sheet.Cells[cnt, 11].Text;
                        redboothData.Assignee = sheet.Cells[cnt, 12].Text;
                        redboothData.Status = sheet.Cells[cnt, 13].Text;
                        if (sheet.Cells[cnt, 14].Text.Length != 0)
                        {
                            redboothData.DueDate = DateTime.ParseExact(sheet.Cells[cnt, 14].Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                        }
                        if (sheet.Cells[cnt, 15].Text.Length != 0)
                        {
                            redboothData.Completed = DateTime.ParseExact(sheet.Cells[cnt, 15].Text, "MM/dd/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                        }
                        redboothData.Tags = sheet.Cells[cnt, 16].Text;

                        listRedboothData.Add(redboothData);

                        cnt++;
                    }


                    string json2 = JsonConvert.SerializeObject(listRedboothData, Formatting.Indented);

                    metadata.ImportedBy = "some user";
                    metadata.MinDate = listRedboothData.Min(x => x.Date.Value).Date;
                    metadata.MaxDate = listRedboothData.Max(x => x.Date.Value).Date;
                    metadata.TimeStamp = DateTime.Now;

                    ret = true;
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothService", "ImportExcelToDataBase", ex.Message, ex.StackTrace);
                }
            });

            return ret;
        }

        public bool SendEmail(string message, string mailTo, List<string> bccStrings)
        {

            bool ret = false;
            try
            {
                MailMessage mail = new MailMessage("redbooth@strix.pt", mailTo);
                mail.Subject = "Strix Redbooth monthly report";
                mail.Body = message;
                foreach (var address in bccStrings)
                {
                    MailAddress bcc = new MailAddress(address);
                    mail.Bcc.Add(bcc);
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;

                smtp.Credentials = new NetworkCredential(
                    "redbooth@strix.pt", "anacleto2020");
                smtp.EnableSsl = true;
                smtp.Send(mail);

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        public async Task<List<TimeReport>> CalculateWorkloadReport(DateTime minDate, DateTime maxDate, bool sendEmails)
        {
            List<TimeReport> report = new List<TimeReport>();
            await Task.Run(() =>
            {
                try
                {

                    List<List<UserID>> TeamNamesList = new List<List<UserID>>();
                    List<RedboothData> alltasksList = new List<RedboothData>();

                    TeamNamesList.Add(StrixTeam.GetBiodiversityInterns(minDate));
                    TeamNamesList.Add(StrixTeam.GetItPeople(minDate));
                    TeamNamesList.Add(StrixTeam.GetBusinessDevPeople(minDate));
                    TeamNamesList = TeamNamesList.Distinct().ToList();
                    foreach (var department in TeamNamesList)
                    {
                        foreach (var userID in department)
                        {
                            TimeReport tr;
                            MakeTimeReport(minDate, maxDate, out alltasksList, userID, out tr);
                            string headingReport = MakeReportHeading(minDate, maxDate, tr);
                            string managerReport = MakeManagerReport(userID, minDate, maxDate, alltasksList);
                            string bulkReport = MakeBulkReport(tr);
                            string text = headingReport + managerReport + bulkReport;
#if DEBUG
                            string path = $"c:/temp/RB_{tr.Name}_{minDate.Year}_{minDate.Month}_{maxDate.Day}.txt";
                            if (!Directory.Exists(Path.GetDirectoryName(path)))
                            {
                                Directory.CreateDirectory(Path.GetDirectoryName(path));
                            };
                            if (!File.Exists(path))
                            {
                                // Create a file to write to.
                                using (StreamWriter sw = File.CreateText(path))
                                {
                                    sw.Write(text);
                                }
                            }
#endif


                            List<string> bccs = new List<string>();

                            List<UserID> managers = new List<UserID>();

                            switch (userID.Department)
                            {
                                case STXDepartment.BioDepartmentInterns:
                                case STXDepartment.BioDepartmentExterns:
                                    managers = StrixTeam.GetBiodiversityManagers(minDate);
                                    break;
                                case STXDepartment.ITDepartment:
                                    managers = StrixTeam.GetTechDevManagers(minDate);
                                    break;
                                case STXDepartment.BusinessDevDepartment:
                                    managers = StrixTeam.GetBusinessDevManagers(minDate);
                                    break;
                                default:
                                    break;
                            }

                            foreach (var address in managers)
                            {
                                bccs.Add(address.Email);
                            }
                            if (sendEmails)
                            {
                                SendEmail(text, userID.Email, bccs);
                            }
#endregion
                        }
                    }
                    //management
                    {
                        List<UserID> upperManagementPeopleList = new List<UserID>();

                        upperManagementPeopleList = StrixTeam.GetManagementPeople(minDate);

                        string managerReport = string.Empty;
                        if (StrixTeam.GetTechDevManagers(minDate).Count > 0)
                        {
                            managerReport = MakeManagerReport(StrixTeam.GetTechDevManagers(minDate).First(), minDate, maxDate, alltasksList);
                        }
                        if (StrixTeam.GetBusinessDevManagers(minDate).Count > 0)
                        {
                            managerReport += MakeManagerReport(StrixTeam.GetBusinessDevManagers(minDate).First(), minDate, maxDate, alltasksList);
                        }
                        if (StrixTeam.GetBiodiversityManagers(minDate).Count > 0)
                        {
                            managerReport += MakeManagerReport(StrixTeam.GetBiodiversityManagers(minDate).First(), minDate, maxDate, alltasksList);
                        }

                        string path = $"c:/temp/RB_Management_{minDate.Year}_{minDate.Month}_{maxDate.Day}.txt";
                        if (!Directory.Exists(Path.GetDirectoryName(path)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(path));
                        };
                        if (!File.Exists(path))
                        {
                            // Create a file to write to.
                            using (StreamWriter sw = File.CreateText(path))
                            {
                                sw.Write(managerReport);
                            }
                        }
                        List<string> bccs = new List<string>();
                        foreach (var manager in upperManagementPeopleList)
                        {
                            if (sendEmails)
                            {
                                SendEmail(managerReport, manager.Email, bccs);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            );
            return report;
        }

        private static string MakeBulkReport(TimeReport tr)
        {
            string heading = "###################################" + Environment.NewLine + "Personal Task List Report:" + Environment.NewLine + "###################################" + Environment.NewLine;
            foreach (var entry in tr.TaskList)
            {
                heading += "Date: \t" + entry.Date.Value.Year + "/" + entry.Date.Value.Month + "/" + entry.Date.Value.Day + "\t" + "Hours.DecimanlMinutes:\t" + entry.Time_Hours.ToString("F2") + "\t" + "Project:\t" + entry.Project + "\t" + "TaskList:\t" + entry.TaskList + "\t" + "Task:\t" + entry.Task + Environment.NewLine;
            }
            return heading;
        }

        private string MakeReportHeading(DateTime minDate, DateTime maxDate, TimeReport tr)
        {
            string text = "###################################" + Environment.NewLine + "Personal Summary:" + Environment.NewLine + "###################################" + Environment.NewLine;
            text += "Name: " + tr.Name + Environment.NewLine;
            text += "Start Date: " + minDate.ToString("dd/MM/yyyy") + Environment.NewLine;
            text += "End Date: " + maxDate.ToString("dd/MM/yyyy") + Environment.NewLine;
            text += "Worked Hours: " + tr.WorkedHours + Environment.NewLine;
            text += "Rest Hours: " + tr.RestHours + Environment.NewLine;
            text += "Leave Hours: " + tr.LeaveHours + Environment.NewLine;
            text += "Total Monthly availabe Hours: " + tr.TotalavailableHours + Environment.NewLine;
            text += Environment.NewLine + Environment.NewLine;
            return text;
        }

        private string MakeManagerReportBioInterns(DateTime minDate, DateTime maxDate)
        {
            string report = "###################################" + Environment.NewLine + "Biodiversity Intern Team Report:" + Environment.NewLine + "###################################" + Environment.NewLine;
            List<KPI> kpis = new List<KPI>();
            kpis = CalculateMetricsBioInterns(minDate).Result;
            foreach (var kpi in kpis)
            {
                report += kpi.Description + "\t" + getKPIValueForMonth(kpi, minDate.Month) + "\t" + kpi.Units + Environment.NewLine;
            }
            return report;
        }

        private string MakeManagerReportBioExterns(DateTime minDate, DateTime maxDate)
        {
            string report = "###################################" + Environment.NewLine + "Biodiversity Extern Team Report:" + Environment.NewLine + "###################################" + Environment.NewLine;
            List<KPI> kpis = new List<KPI>();
            kpis = CalculateMetricsBioExterns(minDate).Result;
            foreach (var kpi in kpis)
            {
                report += kpi.Description + "\t" + getKPIValueForMonth(kpi, minDate.Month) + "\t" + kpi.Units + Environment.NewLine;
            }
            return report;
        }

        private string getKPIValueForMonth(KPI kpi, int month)
        {
            string ret = string.Empty;

            switch (month)
            {
                case 1:
                    ret = kpi.ValueJan;
                    break;
                case 2:
                    ret = kpi.ValueFev;
                    break;
                case 3:
                    ret = kpi.ValueMar;
                    break;
                case 4:
                    ret = kpi.ValueAbr;
                    break;
                case 5:
                    ret = kpi.ValueMai;
                    break;
                case 6:
                    ret = kpi.ValueJun;
                    break;
                case 7:
                    ret = kpi.ValueJul;
                    break;
                case 8:
                    ret = kpi.ValueAgo;
                    break;
                case 9:
                    ret = kpi.ValueSet;
                    break;
                case 10:
                    ret = kpi.ValueOut;
                    break;
                case 11:
                    ret = kpi.ValueNov;
                    break;
                case 12:
                    ret = kpi.ValueDez;
                    break;
            }

            return ret;
        }

        private string MakeManagerReportTDev(DateTime minDate, DateTime maxDate)
        {
            string report = "###################################" + Environment.NewLine + "Technology Dev.Team Report:" + Environment.NewLine + "###################################" + Environment.NewLine;
            List<KPI> kpis = new List<KPI>();
            //kpis = CalculateMetricsTDev(metadataID, minDate, maxDate).Result;
            kpis = CalculateMetricsTDev(minDate).Result;
            foreach (var kpi in kpis)
            {
                report += kpi.Description + "\t" + getKPIValueForMonth(kpi, minDate.Month) + "\t" + kpi.Units + Environment.NewLine;
            }
            return report;
        }

        private string MakeManagerReportBDev(DateTime minDate, DateTime maxDate)
        {
            string report = "###################################" + Environment.NewLine + "Business Dev. Team Report:" + Environment.NewLine + "###################################" + Environment.NewLine;
            List<KPI> kpis = new List<KPI>();
            //kpis = CalculateMetricsBDev(metadataID, minDate, maxDate).Result;
            kpis = CalculateMetricsBDev(minDate).Result;
            foreach (var kpi in kpis)
            {
                report += kpi.Description + "\t" + kpi.Value + "\t" + kpi.Units + Environment.NewLine;
            }
            return report;
        }

        private string MakeManagerReport(UserID userID, DateTime minDate, DateTime maxDate, List<RedboothData> alltasksList)
        {
            string teamReport = string.Empty;
            //check if user ID is a manager
            var managersBio = StrixTeam.GetBiodiversityManagers(minDate);
            foreach (var manager in managersBio)
            {
                if (userID.Name.Contains(manager.Name))
                {
                    teamReport = MakeManagerReportBioInterns(minDate, maxDate) + Environment.NewLine; 
                    teamReport += MakeManagerReportBioExterns(minDate, maxDate) + Environment.NewLine; ;
                    break;
                }
                continue;
            }
            var managersIT = StrixTeam.GetTechDevManagers(minDate);
            foreach (var manager in managersIT)
            {
                if (userID.Name.Contains(manager.Name))
                {
                    teamReport = MakeManagerReportTDev(minDate, maxDate);
                    break;
                }
                continue;
            }
            var managersBDev = StrixTeam.GetBusinessDevManagers(minDate);
            foreach (var manager in managersBDev)
            {
                if (userID.Name.Contains(manager.Name))
                {
                    teamReport = MakeManagerReportBDev(minDate, maxDate);
                    break;
                }
                continue;
            }
            return teamReport;
        }
        private void MakeTimeReport(DateTime minDate, DateTime maxDate, out List<RedboothData> alltasksList, UserID userID, out TimeReport tr)
        {
            tr = new TimeReport();
            alltasksList = listRedboothData.Where(x => x.Name == userID.Name).ToList();
            tr.RestHours = alltasksList.Where(t => t.Task == "Ferias" || t.Task == "Folgas").Sum(x => x.Time_Hours);
            tr.LeaveHours = alltasksList.Where(t => t.Task == "Falta justificada").Sum(x => x.Time_Hours);
            tr.WorkedHours = alltasksList.Where(t => t.Project != "Processo: Ferias e ausencias").Sum(x => x.Time_Hours);
            tr.Name = userID.Name;
            tr.Month = minDate.Month;
            tr.Year = minDate.Year;
            tr.TotalavailableHours = BusinessDaysUntil(minDate, maxDate) * 8;
            tr.TaskList = alltasksList.GroupBy(x => x.Date).ToList().SelectMany(y => y.ToList()).OrderBy(o => o.Date).ToList();
        }

        public async Task<List<KPI>> CalculateMetricsBDev(DateTime minDate)
        {
#region List KPI
            Regex regexOps = new Regex("^[t][0-9]{4}");
            List<KPI> listKPI = new List<KPI>();
            listKPI.Add(new KPI());

            KPI kpi = new KPI();
            kpi.Description = DepartmentKPIs.BusinessDevelopment.RedboothPercentageReportedHours.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.RedboothPercentageReportedHours.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            List<UserID> busDevsAnual = new List<UserID>();
            busDevsAnual.AddRange(StrixTeam.GetBusinessDevPeople(new DateTime(minDate.Year, 1, 1)));


            List<RedboothData> zDataListBusDevsAnual = listRedboothData.Where(x => busDevsAnual.Select(y => y.Name).Contains(x.Name)).ToList();

            List<string> processosDoBDevAnual = zDataListBusDevsAnual.Select(x => x.Project).Distinct().ToList();
            foreach (var processo in processosDoBDevAnual)
            {
                kpi = new KPI();
                kpi.Description = "% de esforço em: " + processo;
                kpi.Units = "%";
                kpi.Department = Departments.BusinessDevelopment;
                listKPI.Add(kpi);
            }

#endregion

            await Task.Run(() =>
            {
                try
                {
                    List<RedboothData> zDataListBusDevs = new List<RedboothData>();
                    List<RedboothData> projectDataOps = new List<RedboothData>();
                    List<RedboothData> notProjectDataOps = new List<RedboothData>();


                    for (int i = 0; i < 12; i++)
                    {
                        List<UserID> busDevsTeamByMonth = new List<UserID>();
                        busDevsTeamByMonth.AddRange(StrixTeam.GetBusinessDevPeople(new DateTime(minDate.Year, i+1, 1)));

                        int month = i + 1;

                        List<UserID> busDevs = new List<UserID>();

                        busDevs.AddRange(StrixTeam.GetBusinessDevPeople(minDate));

                        var temp1 = listRedboothData.Where(x => x.Date.Value.Month == month).ToList();
                        var temp2 = temp1.Where(x => busDevs.Select(y => y.Name).Contains(x.Name)).ToList();
                        zDataListBusDevs = temp2;


                        int cntDaysPeriod = BusinessDaysUntil(new DateTime(minDate.Year, month, 1), new DateTime(minDate.Year, month, DateTime.DaysInMonth(minDate.Year, month)));

                        int totalAvailableHoursMonthBDev = cntDaysPeriod * busDevs.Count * 8;
                        double totalRedboothHoursBDev = zDataListBusDevs.Sum(x => x.Time_Hours);

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.BusinessDevelopment.RedboothPercentageReportedHours.Item1);
                        double value = ((totalRedboothHoursBDev * 100.0) / totalAvailableHoursMonthBDev);
                        if (value != 0)
                        {
                            kpi.Target = 95;
                            if (value >= 95)
                            {
                                kpi.Evaluation = "Above Target";
                            }
                            else
                            {
                                kpi.Evaluation = "Below Target";
                            }
                            if (value <= 80)
                            {
                                kpi.Evaluation = "Critical Target";
                            }
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        List<string> processosDoBDev = zDataListBusDevs.Select(x => x.Project).Distinct().ToList();
                        foreach (var processo in processosDoBDev)
                        {
                            var processTotalHours = zDataListBusDevs.Where(x => x.Project.Contains(processo)).Sum(y => y.Time_Hours);
                            kpi = listKPI.First(x => x.Description == "% de esforço em: " + processo);

                            value = 100 * processTotalHours / totalRedboothHoursBDev;
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }
                    }

                    kpi = new KPI();
                    listKPI.Add(kpi);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });
            ExportKPIsToExcel("BDev_KPIs", minDate, listKPI);
            return listKPI;
        }

        public async Task<List<KPI>> CalculateMetricsTDev(DateTime minDate)
        {

            List<KPI> listKPI = new List<KPI>();
            listKPI.Add(new KPI());

            KPI kpi = new KPI();
            kpi.Description = DepartmentKPIs.IT.RedboothPercentageReportedHours.Item1;
            kpi.Units = DepartmentKPIs.IT.RedboothPercentageReportedHours.Item2;
            kpi.Department = Departments.IT;
            listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DT33";
            kpi.Description = DepartmentKPIs.IT.HoursNotSoftware.Item1;
            kpi.Units = DepartmentKPIs.IT.HoursNotSoftware.Item2;
            kpi.Department = Departments.IT;
            listKPI.Add(kpi);

            List<UserID> tecnologicosAnual = new List<UserID>();
            tecnologicosAnual.AddRange(StrixTeam.GetItPeople(new DateTime(minDate.Year, 1, 1)));
            List<RedboothData> zDataListTecnoAnual = listRedboothData.Where(x => tecnologicosAnual.Select(y => y.Name).Contains(x.Name)).ToList();

            List<string> processosDoTecnoAnual = zDataListTecnoAnual.Select(x => x.Project).Distinct().ToList();
            foreach (var processo in processosDoTecnoAnual)
            {
                kpi = new KPI();
                kpi.Description = "% de esforço em: " + processo;
                kpi.Units = "%";
                kpi.Department = Departments.IT;
                listKPI.Add(kpi);
            }
            kpi = new KPI();
            listKPI.Add(kpi);

            await Task.Run(() =>
            {
                try
                {
                    List<RedboothData> zDataListTecno = new List<RedboothData>();
                    

                    for (int i = 0; i < 12; i++)
                    {
                        List<UserID> tecnologicos = new List<UserID>();
                        tecnologicos.AddRange(StrixTeam.GetItPeople(new DateTime(minDate.Year, i+1, 1)));

                        int month = i + 1;

                        var temp1 = listRedboothData.Where(x => x.Date.Value.Month == month).ToList();
                        var temp2 = temp1.Where(x => tecnologicos.Select(y => y.Name).Contains(x.Name)).ToList();
                        zDataListTecno = temp2;

                        int cntDaysPeriod = BusinessDaysUntil(new DateTime(minDate.Year, month, 1), new DateTime(minDate.Year, month, DateTime.DaysInMonth(minDate.Year, month)));

                        int totalAvailableHoursMonthTecno = cntDaysPeriod * tecnologicos.Count * 8;
                        double totalITHoursNotSoftware = zDataListTecno.Where(x => x.Tags != "Tech---SW").Sum(x => x.Time_Hours);
                        double totalRedboothHoursDtec = zDataListTecno.Sum(x => x.Time_Hours);

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.IT.RedboothPercentageReportedHours.Item1);
                        double value = 0;
                        if (totalRedboothHoursDtec != 0)
                        {
                            value = (totalRedboothHoursDtec * 100.0) / totalAvailableHoursMonthTecno;
                            kpi.Target = 95;
                            if (value >= kpi.Target)
                            {
                                kpi.Evaluation = "Above Target";
                            }
                            else
                            {
                                kpi.Evaluation = "Below Target";
                            }
                            if (value <= 80)
                            {
                                kpi.Evaluation = "Critical Target";
                            }
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.IT.HoursNotSoftware.Item1);
                        if (totalITHoursNotSoftware != 0)
                        {
                            value = totalITHoursNotSoftware / totalAvailableHoursMonthTecno*100;
                            kpi.Target = 400.0 / 1848.0 * tecnologicos.Count*100;
                            if (value > kpi.Target)
                            {
                                kpi.Evaluation = "Above Target";
                            }
                            else
                            {
                                kpi.Evaluation = "Below Target";
                            }
                            if (value <= 50)
                            {
                                kpi.Evaluation = "Critical Target";
                            }
                            kpi.Value = value.ToString("0.####");
                            Tools.SetKPIValue(month, kpi, value);
                        }


                        List<string> processosDoTecno = zDataListTecno.Select(x => x.Project).Distinct().ToList();
                        foreach (var processo in processosDoTecno)
                        {
                            var processTotalHours = zDataListTecno.Where(x => x.Project.Contains(processo)).Sum(y => y.Time_Hours);
                            kpi = listKPI.First(x => x.Description == "% de esforço em: " + processo);
                            if (processTotalHours != 0)
                            {
                                value = 100 * processTotalHours / totalAvailableHoursMonthTecno;
                                kpi.Value = value.ToString("0.##");
                                Tools.SetKPIValue(month, kpi, value);
                            }
                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });
            ExportKPIsToExcel("TDev_KPIs", minDate, listKPI);
            return listKPI;
        }

        private static void ExportKPIsToExcel(string kpiName, DateTime minDate, List<KPI> listKPI)
        {
            string filePath = $"C:\\Temp\\{kpiName}_{minDate.Year}.xlsx";
            int ver = 1;
            while (File.Exists(filePath))
            {
                filePath = $"C:\\Temp\\{kpiName}_{minDate.Year}_{ver++}.xlsx";
            }
            var package = new ExcelPackage(new FileInfo(filePath));
            ExcelWorksheet TDevSheet = package.Workbook.Worksheets.Add("KPIs");
            int col = 1;
            int idx = 1;
            TDevSheet.Cells[idx, col++].Value = "Name";
            TDevSheet.Cells[idx, col++].Value = "Description";
            TDevSheet.Cells[idx, col++].Value = "Jan";
            TDevSheet.Cells[idx, col++].Value = "Fev";
            TDevSheet.Cells[idx, col++].Value = "Mar";
            TDevSheet.Cells[idx, col++].Value = "Abr";
            TDevSheet.Cells[idx, col++].Value = "Mai";
            TDevSheet.Cells[idx, col++].Value = "Jun";
            TDevSheet.Cells[idx, col++].Value = "Jul";
            TDevSheet.Cells[idx, col++].Value = "Ago";
            TDevSheet.Cells[idx, col++].Value = "Set";
            TDevSheet.Cells[idx, col++].Value = "Out";
            TDevSheet.Cells[idx, col++].Value = "Nov";
            TDevSheet.Cells[idx, col++].Value = "Dez";
            TDevSheet.Cells[idx, col++].Value = "Units";
            TDevSheet.Cells[idx, col++].Value = "Target";
            TDevSheet.Cells[idx, col++].Value = "Department";
            idx++;
            foreach (var elem in listKPI)
            {
                col = 1;
                TDevSheet.Cells[idx, col++].Value = elem.Name;
                TDevSheet.Cells[idx, col++].Value = elem.Description;
                TDevSheet.Cells[idx, col++].Value = (elem.ValueJan == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueJan);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueFev == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueFev);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueMar == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueMar);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueAbr == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueAbr);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueMai == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueMai);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueJun == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueJun);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueJul == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueJul);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueAgo == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueAgo);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueSet == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueSet);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueOut == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueOut);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueNov == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueNov);
                TDevSheet.Cells[idx, col++].Value = (elem.ValueDez == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueDez);
                TDevSheet.Cells[idx, col++].Value = elem.Units;
                TDevSheet.Cells[idx, col++].Value = elem.Target;
                TDevSheet.Cells[idx, col++].Value = elem.Department;
                idx++;
            }
            package.Save();
        }

        public async Task<List<KPI>> CalculateMetricsBioInterns(DateTime minDate)
        {
#region List KPI
            Regex regexOps = new Regex("^[t][0-9]{4}");
            List<KPI> listKPI = new List<KPI>();
            listKPI.Add(new KPI());
            listKPI.Add(new KPI() { Name = "OP51" });
            listKPI.Add(new KPI() { Name = "OP50" });
            listKPI.Add(new KPI() { Name = "OP47" });
            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.PercentageHoursLeave.Item1 });
            listKPI.Add(new KPI());

            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.ManDayExecutingProjects.Item1 });
            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.ManDayNotExecutingProjets.Item1 });
            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.ManDayLeave.Item1 });


#region Anual
            List<RedboothData> projectDataOpsAnual = new List<RedboothData>();
            List<RedboothData> notProjectDataOpsAnual = new List<RedboothData>();
            List<RedboothData> LeaveAnual = new List<RedboothData>();

            List<UserID> biodiversityAnual = new List<UserID>();
            biodiversityAnual.AddRange(StrixTeam.GetBiodiversityInterns(new DateTime(minDate.Year, 1, 1)));

            var anualBioDiversityRedboothData = listRedboothData.Where(x => biodiversityAnual.Select(y => y.Name).Contains(x.Name)).ToList();

            projectDataOpsAnual = anualBioDiversityRedboothData.Where(x => regexOps.Match(x.Project).Success == true).ToList();
            notProjectDataOpsAnual = anualBioDiversityRedboothData.Where(x => regexOps.Match(x.Project).Success == false).ToList();
            LeaveAnual = anualBioDiversityRedboothData.Where(x => x.Project.Contains("Ferias e ausencias")).ToList();

            List<string> processosDasOpsAnual = notProjectDataOpsAnual.Select(x => x.Project).Distinct().ToList();

            //listKPI.Add(new KPI());
            //foreach (var processo in processosDasOpsAnual)
            //{
            //    KPI kpi = new KPI();
            //    kpi.Description = "% de esforço em: " + processo;
            //    kpi.Units = "%";
            //    kpi.Department = Departments.BiodiversityInterns;

            //    listKPI.Add(kpi);
            //}

            listKPI.Add(new KPI());
            foreach (var processo in processosDasOpsAnual)
            {
                KPI kpi = new KPI();
                kpi.Description = "H de esforço em: " + processo;
                kpi.Units = "H";
                kpi.Department = Departments.BiodiversityInterns;
                listKPI.Add(kpi);
            }

            listKPI.Add(new KPI());
            foreach (var processo in processosDasOpsAnual)
            {
                KPI kpi = new KPI();
                kpi.Description = "Homem dia de esforço em: " + processo;
                kpi.Units = "Man*Day";
                kpi.Department = Departments.BiodiversityInterns;
                listKPI.Add(kpi);
            }


            listKPI.Add(new KPI());
            var temp = projectDataOpsAnual.GroupBy(x => x.Name);
            foreach (var item in temp)
            {
                KPI kpi = new KPI();
                kpi.Description = "H em Projetos: " + item.Key;
                kpi.Units = "H";
                kpi.Department = Departments.BiodiversityInterns;
                listKPI.Add(kpi);
            }

            listKPI.Add(new KPI());
            foreach (var item in temp)
            {
                KPI kpi = new KPI();
                kpi.Description = "Homens dia em Projetos: " + item.Key;
                kpi.Units = "Man*Day";
                kpi.Department = Departments.BiodiversityInterns;
                listKPI.Add(kpi);
            }


            listKPI.Add(new KPI());
            var temp3 = projectDataOpsAnual.GroupBy(x => x.Project);
            foreach (var item in temp3)
            {
                KPI kpi = new KPI();
                kpi.Description = "Horas em: " + item.Key;
                kpi.Units = "H";
                kpi.Department = Departments.BiodiversityInterns;
                listKPI.Add(kpi);
            }

            listKPI.Add(new KPI());
            foreach (var item in temp3)
            {
                KPI kpi = new KPI();
                kpi.Description = "Homens dia em: " + item.Key;
                kpi.Units = "Man*Day";
                kpi.Department = Departments.BiodiversityInterns;
                listKPI.Add(kpi);
            }


#endregion

#endregion

            await Task.Run(() =>
            {
                try
                {
                    List<List<UserID>> BioTeams = new List<List<UserID>>();

                    var biodiversityTeam = StrixTeam.GetBiodiversityInterns(minDate);

                    List<RedboothData> zDataListBio = new List<RedboothData>();
                    KPI kpi;
                    List<RedboothData> projectDataOps = new List<RedboothData>();
                    List<RedboothData> notProjectDataOps = new List<RedboothData>();
                    List<RedboothData> LeaveDataOps = new List<RedboothData>();
                    int totalAvailableHoursMonthOps;

                    for (int i = 0; i < 12; i++)
                    {
                        List<UserID> biodiversityTeamByMonth = new List<UserID>();
                        biodiversityTeamByMonth.AddRange(StrixTeam.GetBiodiversityInterns(new DateTime(minDate.Year, i+1, 1)));

                        int month = i + 1;
                        {
                            var temp1 = listRedboothData.Where(x => x.Date.Value.Month == month).ToList();
                            var temp2 = temp1.Where(x => biodiversityTeam.Select(y => y.Name).Contains(x.Name)).ToList();
                            zDataListBio = temp2;
                        }

                        int cntDaysPeriod = BusinessDaysUntil(new DateTime(minDate.Year, month, 1), new DateTime(minDate.Year, month, DateTime.DaysInMonth(minDate.Year, month)));

                        projectDataOps = zDataListBio.Where(x => regexOps.Match(x.Project).Success == true).ToList();
                        notProjectDataOps = zDataListBio.Where(x => regexOps.Match(x.Project).Success == false).ToList();
                        LeaveDataOps = LeaveAnual.Where(x => x.Date.Value.Month == month).ToList();

                        double totalRedboothHoursOps = zDataListBio.Sum(x => x.Time_Hours);
                        double totalProjectsHoursOps = projectDataOps.Sum(x => x.Time_Hours);
                        double totalNotProjectsHoursOps = notProjectDataOps.Sum(x => x.Time_Hours);
                        double totalLeaveHoursOps = LeaveDataOps.Sum(x => x.Time_Hours);
                        double totalRegistredHoursWithoutleave = totalProjectsHoursOps + totalNotProjectsHoursOps - totalLeaveHoursOps;
                        totalAvailableHoursMonthOps = cntDaysPeriod * biodiversityTeam.Count * 8;


#region OPs
                        kpi = listKPI.First(x => x.Name == "OP51");
                        kpi.Description = DepartmentKPIs.Biodiversity.RedboothPercentageReportedHours.Item1;
                        kpi.Units = DepartmentKPIs.Biodiversity.RedboothPercentageReportedHours.Item2;
                        kpi.Department = Departments.BiodiversityInterns;
                        double value = (totalRedboothHoursOps * 100.0) / totalAvailableHoursMonthOps;
                        kpi.Target = 95;
                        Tools.SetKPIValue(month, kpi, value);

                        if (value >= kpi.Target)
                        {
                            kpi.Evaluation = "Above Target";
                        }
                        else
                        {
                            kpi.Evaluation = "Below Target";
                        }
                        if (value <= 80)
                        {
                            kpi.Evaluation = "Critical Target";
                        }

                        kpi = listKPI.First(x => x.Name == "OP50");
                        kpi.Description = DepartmentKPIs.Biodiversity.ExecutingProjectsHours.Item1;
                        kpi.Units = DepartmentKPIs.Biodiversity.ExecutingProjectsHours.Item2;
                        kpi.Department = Departments.BiodiversityInterns;
                        value = totalProjectsHoursOps;
                        kpi.Target = totalAvailableHoursMonthOps * 0.7;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);
                        if (value >= kpi.Target)
                        {
                            kpi.Evaluation = "Above Target";
                        }
                        else
                        {
                            kpi.Evaluation = "Below Target";
                        }
                        if (value <= totalAvailableHoursMonthOps * 0.6)
                        {
                            kpi.Evaluation = "Critical Target";
                        }

                        //sum(horas gastas Nao "T"-ferias)+sum(horas gastas "T")

                        kpi = listKPI.First(x => x.Name == "OP47");
                        kpi.Description = DepartmentKPIs.Biodiversity.PercentageHoursNotExecutingProjets.Item1;
                        kpi.Units = DepartmentKPIs.Biodiversity.PercentageHoursNotExecutingProjets.Item2;
                        kpi.Department = Departments.BiodiversityInterns;
                        value = (100 * (totalNotProjectsHoursOps-totalLeaveHoursOps)) / totalRegistredHoursWithoutleave;
                        kpi.Target = 70;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

                        if (value >= kpi.Target)
                        {
                            kpi.Evaluation = "Above Target";
                        }
                        else
                        {
                            kpi.Evaluation = "Below Target";
                        }
                        if (value <= 50)
                        {
                            kpi.Evaluation = "Critical Target";
                        }

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.PercentageHoursLeave.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.PercentageHoursLeave.Item2;
                        kpi.Department = Departments.BiodiversityInterns;
                        value = 100 * LeaveAnual.Where(x => x.Date.Value.Month == month).Sum(y => y.Time_Hours) / totalAvailableHoursMonthOps;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);


                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.ManDayExecutingProjects.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.ManDayExecutingProjects.Item2;
                        kpi.Department = Departments.BiodiversityInterns;
                        kpi.Target = totalAvailableHoursMonthOps * 0.7 / 8.0;
                        value = totalProjectsHoursOps / 8.0;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.ManDayNotExecutingProjets.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.ManDayNotExecutingProjets.Item2;
                        kpi.Department = Departments.BiodiversityInterns;
                        value = totalNotProjectsHoursOps / 8.0;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.ManDayLeave.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.ManDayLeave.Item2;
                        kpi.Department = Departments.BiodiversityInterns;
                        value = totalLeaveHoursOps / 8.0;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

#endregion


#region Projects
                        //foreach (var processo in processosDasOpsAnual)
                        //{
                        //    var processTotalHours = zDataListBio.Where(x => x.Project.Contains(processo) && x.Date.Value.Month == month).Sum(y => y.Time_Hours);
                        //    kpi = listKPI.First(x => x.Description == "% de esforço em: " + processo);
                        //    value = (100 * processTotalHours / totalAvailableHoursMonthOps);
                        //    kpi.Value = value.ToString("0.##");
                        //    Tools.SetKPIValue(month, kpi, value);
                        //}
                        foreach (var processo in processosDasOpsAnual)
                        {
                            var processTotalHours = zDataListBio.Where(x => x.Project.Contains(processo) && x.Date.Value.Month == month).Sum(y => y.Time_Hours);
                            kpi = listKPI.First(x => x.Description == "H de esforço em: " + processo);
                            value = processTotalHours;
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var processo in processosDasOpsAnual)
                        {
                            var processTotalHours = zDataListBio.Where(x => x.Project.Contains(processo) && x.Date.Value.Month == month).Sum(y => y.Time_Hours);
                            kpi = listKPI.First(x => x.Description == "Homem dia de esforço em: " + processo);
                            value = processTotalHours / 8.0;
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Name))
                        {
                            kpi = listKPI.First(x => x.Description == "H em Projetos: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours));
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Name))
                        {
                            kpi = listKPI.First(x => x.Description == "Homens dia em Projetos: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours) / 8.0);
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Project))
                        {
                            kpi = listKPI.First(x => x.Description == "Horas em: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours));
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Project))
                        {
                            kpi = listKPI.First(x => x.Description == "Homens dia em: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours) / 8.0);
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }
#endregion
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });
            ExportKPIsToExcel("InternsBioEco_KPIs", minDate, listKPI);
            return listKPI;
        }

        public async Task<List<KPI>> CalculateMetricsBioExterns(DateTime minDate)
        {
#region List KPI
            Regex regexOps = new Regex("^[t][0-9]{4}");
            List<KPI> listKPI = new List<KPI>();
            listKPI.Add(new KPI());

            listKPI.Add(new KPI() { Name = "OP51" });
            listKPI.Add(new KPI() { Name = "OP50" });
            listKPI.Add(new KPI() { Name = "OP47" });
            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.PercentageHoursLeave.Item1 });
            listKPI.Add(new KPI());

            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.ManDayExecutingProjects.Item1 });
            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.ManDayNotExecutingProjets.Item1 });
            listKPI.Add(new KPI() { Description = DepartmentKPIs.Biodiversity.ManDayLeave.Item1 });


#region Anual
            List<RedboothData> projectDataOpsAnual = new List<RedboothData>();
            List<RedboothData> notProjectDataOpsAnual = new List<RedboothData>();
            List<RedboothData> LeaveAnual = new List<RedboothData>();

            List<UserID> biodiversityAnual = new List<UserID>();
            biodiversityAnual.AddRange(StrixTeam.GetBiodiversityExterns(new DateTime(minDate.Year, 1, 1)));
            var anualBioDiversityRedboothData = listRedboothData.Where(x => biodiversityAnual.Select(y => y.Name).Contains(x.Name)).ToList();

            projectDataOpsAnual = anualBioDiversityRedboothData.Where(x => regexOps.Match(x.Project).Success == true).ToList();
            notProjectDataOpsAnual = anualBioDiversityRedboothData.Where(x => regexOps.Match(x.Project).Success == false).ToList();
            LeaveAnual = anualBioDiversityRedboothData.Where(x => x.Project.Contains("Ferias e ausencias")).ToList();

            List<string> processosDasOpsAnual = notProjectDataOpsAnual.Select(x => x.Project).Distinct().ToList();

            //listKPI.Add(new KPI());
            //foreach (var processo in processosDasOpsAnual)
            //{
            //    KPI kpi = new KPI();
            //    kpi.Description = "% de esforço em: " + processo;
            //    kpi.Units = "%";
            //    kpi.Department = Departments.BiodiversityExterns;

            //    listKPI.Add(kpi);
            //}

            listKPI.Add(new KPI());
            foreach (var processo in processosDasOpsAnual)
            {
                KPI kpi = new KPI();
                kpi.Description = "H de esforço em: " + processo;
                kpi.Units = "H";
                kpi.Department = Departments.BiodiversityExterns;
                listKPI.Add(kpi);
            }

            listKPI.Add(new KPI());
            foreach (var processo in processosDasOpsAnual)
            {
                KPI kpi = new KPI();
                kpi.Description = "Homem dia de esforço em: " + processo;
                kpi.Units = "Man*Day";
                kpi.Department = Departments.BiodiversityExterns;
                listKPI.Add(kpi);
            }


            listKPI.Add(new KPI());
            var temp = projectDataOpsAnual.GroupBy(x => x.Name);
            foreach (var item in temp)
            {
                KPI kpi = new KPI();
                kpi.Description = "H em Projetos: " + item.Key;
                kpi.Units = "H";
                kpi.Department = Departments.BiodiversityExterns;
                listKPI.Add(kpi);
            }

            listKPI.Add(new KPI());
            foreach (var item in temp)
            {
                KPI kpi = new KPI();
                kpi.Description = "Homens dia em Projetos: " + item.Key;
                kpi.Units = "Man*Day";
                kpi.Department = Departments.BiodiversityExterns;
                listKPI.Add(kpi);
            }


            listKPI.Add(new KPI());
            var temp3 = projectDataOpsAnual.GroupBy(x => x.Project);
            foreach (var item in temp3)
            {
                KPI kpi = new KPI();
                kpi.Description = "Horas em: " + item.Key;
                kpi.Units = "H";
                kpi.Department = Departments.BiodiversityExterns;
                listKPI.Add(kpi);
            }

            listKPI.Add(new KPI());
            foreach (var item in temp3)
            {
                KPI kpi = new KPI();
                kpi.Description = "Homens dia em: " + item.Key;
                kpi.Units = "Man*Day";
                kpi.Department = Departments.BiodiversityExterns;
                listKPI.Add(kpi);
            }


#endregion

#endregion

            await Task.Run(() =>
            {
                try
                {
                    List<UserID> externBioTeam = new List<UserID>();        
                    externBioTeam.AddRange(StrixTeam.GetBiodiversityExterns(minDate));


                    List<RedboothData> zDataListBio = new List<RedboothData>();
                    KPI kpi;
                    List<RedboothData> projectDataOps = new List<RedboothData>();
                    List<RedboothData> notProjectDataOps = new List<RedboothData>();
                    List<RedboothData> LeaveDataOps = new List<RedboothData>();
                    int totalAvailableHoursMonthOps;

                    for (int i = 0; i < 12; i++)
                    {

                        List<UserID> biodiversityMonthlyTeam = new List<UserID>();
                        biodiversityMonthlyTeam.AddRange(StrixTeam.GetBiodiversityExterns(new DateTime(minDate.Year, i+1, 1)));


                        int month = i + 1;
                        {
                            var temp1 = listRedboothData.Where(x => x.Date.Value.Month == month).ToList();
                            var temp2 = temp1.Where(x => externBioTeam.Select(y => y.Name).Contains(x.Name)).ToList();
                            zDataListBio = temp2;
                        }

                        int cntDaysPeriod = BusinessDaysUntil(new DateTime(minDate.Year, month, 1), new DateTime(minDate.Year, month, DateTime.DaysInMonth(minDate.Year, month)));

                        projectDataOps = zDataListBio.Where(x => regexOps.Match(x.Project).Success == true).ToList();
                        notProjectDataOps = zDataListBio.Where(x => regexOps.Match(x.Project).Success == false).ToList();
                        LeaveDataOps = LeaveAnual.Where(x => x.Date.Value.Month == month).ToList();

                        double totalRedboothHoursOps = zDataListBio.Sum(x => x.Time_Hours);
                        double totalProjectsHoursOps = projectDataOps.Sum(x => x.Time_Hours);
                        double totalNotProjectsHoursOps = notProjectDataOps.Sum(x => x.Time_Hours);
                        double totalLeaveHoursOps = LeaveDataOps.Sum(x => x.Time_Hours);

                        totalAvailableHoursMonthOps = cntDaysPeriod * externBioTeam.Count * 8;


#region OPs
                        kpi = listKPI.First(x => x.Name == "OP51");
                        kpi.Description = DepartmentKPIs.Biodiversity.RedboothPercentageReportedHours.Item1;
                        kpi.Units = DepartmentKPIs.Biodiversity.RedboothPercentageReportedHours.Item2;
                        kpi.Department = Departments.BiodiversityExterns;
                        double value = (totalRedboothHoursOps * 100.0) / totalAvailableHoursMonthOps;
                        kpi.Target = 95;
                        Tools.SetKPIValue(month, kpi, value);

                        if (value >= kpi.Target)
                        {
                            kpi.Evaluation = "Above Target";
                        }
                        else
                        {
                            kpi.Evaluation = "Below Target";
                        }
                        if (value <= 80)
                        {
                            kpi.Evaluation = "Critical Target";
                        }

                        kpi = listKPI.First(x => x.Name == "OP50");
                        kpi.Description = DepartmentKPIs.Biodiversity.ExecutingProjectsHours.Item1;
                        kpi.Units = DepartmentKPIs.Biodiversity.ExecutingProjectsHours.Item2;
                        kpi.Department = Departments.BiodiversityExterns;
                        value = totalProjectsHoursOps;
                        kpi.Target = totalAvailableHoursMonthOps * 0.9;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);
                        if (value >= kpi.Target)
                        {
                            kpi.Evaluation = "Above Target";
                        }
                        else
                        {
                            kpi.Evaluation = "Below Target";
                        }
                        if (value <= totalAvailableHoursMonthOps * 0.6)
                        {
                            kpi.Evaluation = "Critical Target";
                        }



                        kpi = listKPI.First(x => x.Name == "OP47");
                        kpi.Description = DepartmentKPIs.Biodiversity.PercentageHoursNotExecutingProjets.Item1;
                        kpi.Units = DepartmentKPIs.Biodiversity.PercentageHoursNotExecutingProjets.Item2;
                        kpi.Department = Departments.BiodiversityExterns;
                        value = (100 * totalNotProjectsHoursOps) / totalAvailableHoursMonthOps;
                        kpi.Target = 70;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

                        if (value >= kpi.Target)
                        {
                            kpi.Evaluation = "Above Target";
                        }
                        else
                        {
                            kpi.Evaluation = "Below Target";
                        }
                        if (value <= 50)
                        {
                            kpi.Evaluation = "Critical Target";
                        }

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.PercentageHoursLeave.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.PercentageHoursLeave.Item2;
                        kpi.Department = Departments.BiodiversityExterns;
                        value = 100 * LeaveAnual.Where(x => x.Date.Value.Month == month).Sum(y => y.Time_Hours) / totalAvailableHoursMonthOps;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);


                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.ManDayExecutingProjects.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.ManDayExecutingProjects.Item2;
                        kpi.Department = Departments.BiodiversityExterns;
                        kpi.Target = totalAvailableHoursMonthOps * 0.9 / 8.0;
                        value = totalProjectsHoursOps / 8.0;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.ManDayNotExecutingProjets.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.ManDayNotExecutingProjets.Item2;
                        kpi.Department = Departments.BiodiversityExterns;
                        value = totalNotProjectsHoursOps / 8.0;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

                        kpi = listKPI.First(x => x.Description == DepartmentKPIs.Biodiversity.ManDayLeave.Item1);
                        kpi.Units = DepartmentKPIs.Biodiversity.ManDayLeave.Item2;
                        kpi.Department = Departments.BiodiversityExterns;
                        value = totalLeaveHoursOps / 8.0;
                        kpi.Value = value.ToString("0.##");
                        Tools.SetKPIValue(month, kpi, value);

#endregion


#region Projects
                        //foreach (var processo in processosDasOpsAnual)
                        //{
                        //    var processTotalHours = zDataListBio.Where(x => x.Project.Contains(processo) && x.Date.Value.Month == month).Sum(y => y.Time_Hours);
                        //    kpi = listKPI.First(x => x.Description == "% de esforço em: " + processo);
                        //    value = (100 * processTotalHours / totalAvailableHoursMonthOps);
                        //    kpi.Value = value.ToString("0.##");
                        //    Tools.SetKPIValue(month, kpi, value);
                        //}
                        foreach (var processo in processosDasOpsAnual)
                        {
                            var processTotalHours = zDataListBio.Where(x => x.Project.Contains(processo) && x.Date.Value.Month == month).Sum(y => y.Time_Hours);
                            kpi = listKPI.First(x => x.Description == "H de esforço em: " + processo);
                            value = processTotalHours;
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var processo in processosDasOpsAnual)
                        {
                            var processTotalHours = zDataListBio.Where(x => x.Project.Contains(processo) && x.Date.Value.Month == month).Sum(y => y.Time_Hours);
                            kpi = listKPI.First(x => x.Description == "Homem dia de esforço em: " + processo);
                            value = processTotalHours / 8.0;
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Name))
                        {
                            kpi = listKPI.First(x => x.Description == "H em Projetos: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours));
                            kpi.Target = cntDaysPeriod * 8 * 0.9;
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Name))
                        {
                            kpi = listKPI.First(x => x.Description == "Homens dia em Projetos: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours) / 8.0);
                            kpi.Value = value.ToString("0.##");
                            kpi.Target = cntDaysPeriod * 0.9;
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Project))
                        {
                            kpi = listKPI.First(x => x.Description == "Horas em: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours));
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }

                        foreach (var item in projectDataOps.GroupBy(x => x.Project))
                        {
                            kpi = listKPI.First(x => x.Description == "Homens dia em: " + item.Key);

                            value = (item.Sum(x => x.Time_Hours) / 8.0);
                            kpi.Value = value.ToString("0.##");
                            Tools.SetKPIValue(month, kpi, value);
                        }
#endregion

                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });
            ExportKPIsToExcel("ExternsBioEco_KPIs", minDate, listKPI);
            return listKPI;
        }

        public Tuple<DateTime, DateTime> GetTimeWindowForImportedData()
        {
            DateTime dt1 = listRedboothData.Where(y => y.Date != null).Min(x => x.Date.Value);
            DateTime dt2 = listRedboothData.Where(y => y.Date != null).Max(x => x.Date.Value);
            Tuple<DateTime, DateTime> ret = new Tuple<DateTime, DateTime>(dt1, dt2);

            return ret;
        }

#region Private methods

        private double getTimeSpanFromHMS(string timeSpanString)
        {
            int h = 0, m = 0, s = 0, length;
            string[] arraySplit = timeSpanString.Split(' ');

            length = arraySplit.Length;

            switch (length)
            {
                case 1:
                    s = Int32.Parse(arraySplit[0].Remove(arraySplit[0].IndexOf('s')));
                    break;
                case 2:
                    m = Int32.Parse(arraySplit[0].Remove(arraySplit[0].IndexOf('m')));
                    s = Int32.Parse(arraySplit[1].Remove(arraySplit[1].IndexOf('s')));
                    break;
                case 3:
                    h = Int32.Parse(arraySplit[0].Remove(arraySplit[0].IndexOf('h')));
                    m = Int32.Parse(arraySplit[1].Remove(arraySplit[1].IndexOf('m')));
                    s = Int32.Parse(arraySplit[2].Remove(arraySplit[2].IndexOf('s')));
                    break;
            }
            TimeSpan timeSpan = new TimeSpan(h, m, s);

            return timeSpan.Hours + timeSpan.Minutes / 60.0 + timeSpan.Seconds / 3600.0;
        }

        private double getTimeSpanFromDHM(string timeSpanString)
        {
            int d = 0, h = 0, m = 0, length;
            string[] arraySplit = timeSpanString.Split(' ');

            length = arraySplit.Length;

            switch (length)
            {
                case 1:
                    m = Int32.Parse(arraySplit[0].Remove(arraySplit[0].IndexOf('m')));
                    break;
                case 2:
                    h = Int32.Parse(arraySplit[0].Remove(arraySplit[0].IndexOf('h')));
                    m = Int32.Parse(arraySplit[1].Remove(arraySplit[1].IndexOf('m')));
                    break;
                case 3:
                    d = Int32.Parse(arraySplit[0].Remove(arraySplit[0].IndexOf('d')));
                    h = Int32.Parse(arraySplit[1].Remove(arraySplit[1].IndexOf('h')));
                    m = Int32.Parse(arraySplit[2].Remove(arraySplit[2].IndexOf('m')));
                    break;
            }
            TimeSpan timeSpan = new TimeSpan(d, h, m, 0);

            return timeSpan.Hours + timeSpan.Minutes / 60.0 + timeSpan.Seconds / 3600.0;
        }

        private int BusinessDaysUntil(DateTime firstDay, DateTime lastDay)
        {
            string holidays = ConfigurationManager.AppSettings["Holidays"];
            string[] holidaysSplit = holidays.Split(';');

            firstDay = firstDay.Date;
            lastDay = lastDay.Date;
            if (firstDay > lastDay)
                throw new ArgumentException("Incorrect last day " + lastDay);

            TimeSpan span = lastDay - firstDay;
            int businessDays = span.Days + 1;
            int fullWeekCount = businessDays / 7;
            // find out if there are weekends during the time exceedng the full weeks
            if (businessDays > fullWeekCount * 7)
            {
                // we are here to find out if there is a 1-day or 2-days weekend
                // in the time interval remaining after subtracting the complete weeks
                int firstDayOfWeek = firstDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)firstDay.DayOfWeek;
                int lastDayOfWeek = lastDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)lastDay.DayOfWeek;

                if (lastDayOfWeek < firstDayOfWeek)
                    lastDayOfWeek += 7;
                if (firstDayOfWeek <= 6)
                {
                    if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                        businessDays -= 2;
                    else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                        businessDays -= 1;
                }
                else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                    businessDays -= 1;
            }

            // subtract the weekends during the full weeks in the interval
            businessDays -= fullWeekCount + fullWeekCount;

            // subtract the number of bank holidays during the time interval
            foreach (string bankHoliday in holidaysSplit)
            {
                if (bankHoliday.Length > 0)
                {
                    DateTime bh = new DateTime(firstDay.Year, Int32.Parse(bankHoliday.Substring(2, 2)), Int32.Parse(bankHoliday.Substring(0, 2)));
                    if (firstDay <= bh && bh <= lastDay)
                        --businessDays;
                }
            }

            return businessDays;
        }

#endregion
    }
}
