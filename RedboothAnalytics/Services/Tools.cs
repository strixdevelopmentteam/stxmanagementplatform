﻿using Analytics.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analytics.Services
{
    public class Tools
    {
        public static void SetKPIValue(int month, KPI kpi, double value)
        {
            switch (month)
            {
                case 1:
                    kpi.ValueJan = value.ToString("0.##");
                    break;

                case 2:
                    kpi.ValueFev = value.ToString("0.##");
                    break;

                case 3:
                    kpi.ValueMar = value.ToString("0.##");
                    break;

                case 4:
                    kpi.ValueAbr = value.ToString("0.##");
                    break;

                case 5:
                    kpi.ValueMai = value.ToString("0.##");
                    break;

                case 6:
                    kpi.ValueJun = value.ToString("0.##");
                    break;

                case 7:
                    kpi.ValueJul = value.ToString("0.##");
                    break;

                case 8:
                    kpi.ValueAgo = value.ToString("0.##");
                    break;

                case 9:
                    kpi.ValueSet = value.ToString("0.##");
                    break;

                case 10:
                    kpi.ValueOut = value.ToString("0.##");
                    break;

                case 11:
                    kpi.ValueNov = value.ToString("0.##");
                    break;

                case 12:
                    kpi.ValueDez = value.ToString("0.##");
                    break;
            }
        }

        public static void SetKPIValue(int month, KPI kpi, string value)
        {
            switch (month)
            {
                case 1:
                    kpi.ValueJan = value;
                    break;

                case 2:
                    kpi.ValueFev = value;
                    break;

                case 3:
                    kpi.ValueMar = value;
                    break;

                case 4:
                    kpi.ValueAbr = value;
                    break;

                case 5:
                    kpi.ValueMai = value;
                    break;

                case 6:
                    kpi.ValueJun = value;
                    break;

                case 7:
                    kpi.ValueJul = value;
                    break;

                case 8:
                    kpi.ValueAgo = value;
                    break;

                case 9:
                    kpi.ValueSet = value;
                    break;

                case 10:
                    kpi.ValueOut = value;
                    break;

                case 11:
                    kpi.ValueNov = value;
                    break;

                case 12:
                    kpi.ValueDez = value;
                    break;
            }
        }
    }
}
