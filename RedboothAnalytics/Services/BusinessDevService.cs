﻿using Analytics.Models;
using Analytics.People;
using Common.Logs;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Analytics.Services
{
    public class BusinessDevService
    {
        #region Fields
        List<BusinessDevData> listBusinessDevData = new List<BusinessDevData>();
        List<BusinessDevData> listBusinessDevDataYear = new List<BusinessDevData>();
        ExcelWorksheet businessDevSheet;
        #endregion

        #region Constructor
        public BusinessDevService()
        {
        }
        #endregion

        #region Public methods
        public async Task<bool> ImportExcel(string filePath)
        {
            bool ret = false;
            listBusinessDevData = new List<BusinessDevData>();
            listBusinessDevDataYear = new List<BusinessDevData>();
            await Task.Run(() =>
            {
                try
                {
                    var package = new ExcelPackage(new FileInfo(filePath));
                    businessDevSheet = package.Workbook.Worksheets[1];
                    ret = true;
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothService", "ImportExcelToDataBase", ex.Message, ex.StackTrace);
                }
            });

            return ret;
        }

         


        private void ParseBusinessDevSheet(int year, int month)
        {
            listBusinessDevDataYear = new List<BusinessDevData>();
            listBusinessDevData = new List<BusinessDevData>();
            bool isStarted = false;
            int cnt = 2;
            try
            {
                while (true)
                {
                    BusinessDevData businessDevData = new BusinessDevData();

                    if (isStarted && businessDevSheet.Cells[cnt, 3].Text.Length == 0)
                    {
                        break;
                    }

                    if (businessDevSheet.Cells[cnt, 10].Text.Length == 10
                    && DateTime.ParseExact(businessDevSheet.Cells[cnt, 10].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture).Year == year)
                    {
                        isStarted = true;
                        businessDevData.Proposal = businessDevSheet.Cells[cnt, 3].Text;
                        businessDevData.Client = businessDevSheet.Cells[cnt, 5].Text;

                        double value;
                        double.TryParse(businessDevSheet.Cells[cnt, 6].Text.Replace("€", ""), NumberStyles.Any, CultureInfo.CurrentCulture, out value);
                        businessDevData.Value = value;

                        double manDay;
                        double.TryParse(businessDevSheet.Cells[cnt, 7].Text, NumberStyles.Any, CultureInfo.CurrentCulture, out manDay);
                        businessDevData.ManDay = manDay;

                        double overhead;
                        double.TryParse(businessDevSheet.Cells[cnt, 8].Text.Replace("€", ""), NumberStyles.Any, CultureInfo.CurrentCulture, out overhead);
                        businessDevData.Overhead = overhead;

                        double margin;
                        double.TryParse(businessDevSheet.Cells[cnt, 9].Text.Replace("%", ""), NumberStyles.Any, CultureInfo.CurrentCulture, out margin);
                        businessDevData.Margin = margin;

                        if (businessDevSheet.Cells[cnt, 10].Text.Length != 0)
                        {
                            businessDevData.DateSent = DateTime.ParseExact(businessDevSheet.Cells[cnt, 10].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }

                        businessDevData.Area = businessDevSheet.Cells[cnt, 11].Text;
                        businessDevData.NewClient = businessDevSheet.Cells[cnt, 12].Text == "S" ? true : false;

                        businessDevData.Adjudicated = businessDevSheet.Cells[cnt, 13].Text == "S" ? true : false;

                        DateTime tempDate;
                        if (businessDevSheet.Cells[cnt, 14].Text.Length != 0 && DateTime.TryParse(businessDevSheet.Cells[cnt, 14].Text, out tempDate))
                        {
                            try
                            {

                                businessDevData.DateAdjudicated = DateTime.ParseExact(getDateTimeStringFromStringPT(businessDevSheet.Cells[cnt, 14].Text), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            }
                            catch (Exception ex)
                            {

                                throw;
                            }
                        }

                        if (DateTime.ParseExact(businessDevSheet.Cells[cnt, 10].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture).Month == month)
                        {
                            listBusinessDevData.Add(businessDevData);
                        }
                        if (DateTime.ParseExact(businessDevSheet.Cells[cnt, 10].Text, "dd-MM-yyyy", CultureInfo.InvariantCulture).Month <= month)
                        {
                            listBusinessDevDataYear.Add(businessDevData);
                        }
                    }
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "BusinessDeccService", "ParseBusinessDevSheet", ex.Message, ex.StackTrace);
            }
        }

        double GetAccumulatedTarget(double target, int month)
        {
            return target / 12 * month;
        }

        public async Task<List<KPI>> CalculateMetrics(int year)
        {

            /*
DN14	Nº propostas emitidas
DN15	Valor total de propostas emitidas
DN16	Nº propostas adjudicadas
DN17	Valor total de propostas adjudicadas
DN18	Taxa global de sucesso das propostas
DN19	N.º homens/dia adjudicados 
DN20	Net profit
DN21    Nº de novos clientes
            */
            List<KPI> listKPI = new List<KPI>();
            KPI kpi = new KPI();
            kpi.Name = "DN14";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.TotalNumberOfProposals.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.TotalNumberOfProposals.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DN15";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.TotalValueOfProposals.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.TotalValueOfProposals.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DN16";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.TotalNumberOfAdjudicatedProposals.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.TotalNumberOfAdjudicatedProposals.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DN17";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.TotalValueOfAdjudicatedProposals.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.TotalValueOfAdjudicatedProposals.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DN18";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.RatioOfAdjudicatedProposals.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.RatioOfAdjudicatedProposals.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DN19";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.ManDayAdjudicated.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.ManDayAdjudicated.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DN20";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.NetProfit.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.NetProfit.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);



            //kpi = new KPI();
            //kpi.Name = "DN22";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.AvgMarginBirdtrack.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.AvgMarginBirdtrack.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN23";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.AvgMarginEIA.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.AvgMarginEIA.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN24";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.AvgMarginEcologie.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.AvgMarginEcologie.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN25";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.AvgMarginOthers.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.AvgMarginOthers.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.AvgMarginTotal.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.AvgMarginTotal.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN26";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.RacioOfExistingClient.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.RacioOfExistingClient.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            kpi = new KPI();
            kpi.Name = "DN21";
            kpi.Description = DepartmentKPIs.BusinessDevelopment.NumberNewCustomers.Item1;
            kpi.Units = DepartmentKPIs.BusinessDevelopment.NumberNewCustomers.Item2;
            kpi.Department = Departments.BusinessDevelopment;
            listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN14b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyTotalNumberOfProposals.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyTotalNumberOfProposals.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN15b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyTotalValueOfProposals.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyTotalValueOfProposals.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN16b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyTotalValueOfAdjudicatedProposals.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyTotalValueOfAdjudicatedProposals.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "_b1";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyRatioOfAdjudicatedValueProposals.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyRatioOfAdjudicatedValueProposals.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN17b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsBirdtrack.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsBirdtrack.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN18b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsEiaStudies.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsEiaStudies.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN19b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsEcologie.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsEcologie.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN20b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsOthers.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsOthers.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "b2";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsTotal.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfProposalsTotal.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN21b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyManDayAdjudicated.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyManDayAdjudicated.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN22b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginBirdtrack.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginBirdtrack.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN23b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginEIA.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginEIA.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN24b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginEcologie.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginEcologie.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN25b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginOthers.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginOthers.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "b3";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginTotal.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyAvgMarginTotal.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN26b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfExistingClient.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyRacioOfExistingClient.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            //kpi = new KPI();
            //kpi.Name = "DN27b";
            //kpi.Description = DepartmentKPIs.BusinessDevelopment.MonthlyNumberNewCustomers.Item1;
            //kpi.Units = DepartmentKPIs.BusinessDevelopment.MonthlyNumberNewCustomers.Item2;
            //kpi.Department = Departments.BusinessDevelopment;
            //listKPI.Add(kpi);

            for (int i = 0; i < 12; i++)
            {
                int month = i + 1;
                ParseBusinessDevSheet(year, month);

                string valueString;
                var totalApprovedProposalsCount = listBusinessDevDataYear.Where(y => y.Adjudicated == true).ToList().Count;
                var totalApprovedProposalsValue = listBusinessDevDataYear.Where(y => y.Adjudicated == true).Select(x => x.Value).Sum();
                var uniqueProposalsYear = listBusinessDevDataYear.Distinct().ToList();
                var totalIssuedProposalsValue = listBusinessDevDataYear.Select(x => x.Value).Sum();
                var emitidasBT = listBusinessDevDataYear.Where(z => z.Area == "Birdtrack").Select(y => y.Proposal).Distinct().ToList().Count;
                var emitidasEIA = listBusinessDevDataYear.Where(z => z.Area == "EIA/Estudos").Select(y => y.Proposal).Distinct().ToList().Count;
                var emitidasEcologia = listBusinessDevDataYear.Where(z => z.Area == "Ecologia").Select(y => y.Proposal).Distinct().ToList().Count;
                var emitidasOutros = listBusinessDevDataYear.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia").Select(z => z.Proposal).Distinct().ToList().Count;
                var emitidasTodos = listBusinessDevDataYear.Select(z => z.Proposal).Distinct().ToList().Count;
                var valueD = listBusinessDevDataYear.Where(y => y.Adjudicated == true).Sum(x => x.ManDay);
                var monthlyUniqueProposals = listBusinessDevData.Distinct().ToList();
                var monthlyTotalIssuedProposalsValue = listBusinessDevData.Select(x => x.Value).Sum();
                var monthlyTotalApprovedProposalsValue = listBusinessDevData.Where(y => y.Adjudicated == true).Select(x => x.Value).Sum();
                var monthlyEmitidasBT = listBusinessDevData.Where(z => z.Area == "Birdtrack").Select(y => y.Proposal).Distinct().ToList().Count;
                var monthlyEmitidasEIA = listBusinessDevData.Where(z => z.Area == "EIA/Estudos").Select(y => y.Proposal).Distinct().ToList().Count;
                var monthlyEmitidasEcologia = listBusinessDevData.Where(z => z.Area == "Ecologia").Select(y => y.Proposal).Distinct().ToList().Count;
                var monthlyEmitidasOutros = listBusinessDevData.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia").Select(z => z.Proposal).Distinct().ToList().Count;
                var monthlyEmitidasTodos = listBusinessDevData.Select(z => z.Proposal).Distinct().ToList().Count;

                if (listBusinessDevData.Count != 0)
                {
                    await Task.Run(() =>
                    {
                        try
                        {
                            //Nº propostas emitidas
                            kpi = listKPI.First(x => x.Name == "DN14");
                            kpi.Target = GetAccumulatedTarget(60, month);
                            var value = uniqueProposalsYear.Select(y => y.Proposal).ToList().Count;
                            EvaluateValueBusinessPositiveBetter(kpi, value, kpi.Target);
                            Tools.SetKPIValue(month, kpi, value);
                            value = 0;
                            valueString = "";

                            //Valor total de propostas emitidas
                            kpi = listKPI.First(x => x.Name == "DN15");
                            valueString = totalIssuedProposalsValue.ToString("0,0.00", CultureInfo.InvariantCulture);// +"€";
                            kpi.Target = GetAccumulatedTarget(3000000, month);
                            EvaluateValueBusinessPositiveBetter(kpi, totalIssuedProposalsValue, kpi.Target);
                            Tools.SetKPIValue(month, kpi, valueString);
                            value = 0;
                            valueString = "";

                            //Total de propostas adjudicadas
                            kpi = listKPI.First(x => x.Name == "DN16");
                            valueString = totalApprovedProposalsCount.ToString();// +"€";
                            kpi.Target = GetAccumulatedTarget(4, month);
                            EvaluateValueBusinessPositiveBetter(kpi, totalApprovedProposalsValue, kpi.Target);
                            Tools.SetKPIValue(month, kpi, valueString);
                            value = 0;
                            valueString = "";

                            // total de propostas adjudicadas valor
                            kpi = listKPI.First(x => x.Name == "DN17");
                            valueString = totalApprovedProposalsValue.ToString("0,0.00", CultureInfo.InvariantCulture);// +"€";
                            Tools.SetKPIValue(month, kpi, valueString);
                            valueString = "";


                            // ratio entre valor toatal de propostas adjudicado/emitido
                            kpi = listKPI.First(x => x.Name == "DN18");
                            //kpi = listKPI.First(x => x.Description == DepartmentKPIs.BusinessDevelopment.RatioOfAdjudicatedValueProposals.Item1);
                            valueString = ((double)totalApprovedProposalsCount / Math.Max(emitidasTodos, 1) * 100).ToString("F2", CultureInfo.InvariantCulture);// + "%";
                            Tools.SetKPIValue(month, kpi, valueString);
                            value = 0;
                            valueString = "";

                            ////N.º de propostas adjudicadas / emitidas Birdtrack
                            //kpi = listKPI.First(x => x.Name == "DN17");
                            //valueString = (listBusinessDevDataYear.Where(y => y.Area == "Birdtrack" && y.Adjudicated == true).Count() * 100.0 / Math.Max(emitidasBT, 1)).ToString("0.##");
                            //kpi.Target = 40;
                            //EvaluateValueBusinessPositiveBetter(kpi, emitidasBT, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////EIA/Estudos
                            //kpi = listKPI.First(x => x.Name == "DN18");
                            //valueString = (listBusinessDevDataYear.Where(y => y.Area == "EIA/Estudos" && y.Adjudicated == true).Count() * 100.0 / Math.Max(emitidasEIA, 1)).ToString("0.##");
                            //kpi.Target = 30;
                            //EvaluateValueBusinessPositiveBetter(kpi, emitidasEIA, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Ecologia
                            //kpi = listKPI.First(x => x.Name == "DN19");
                            //valueString = (listBusinessDevDataYear.Where(y => y.Area == "Ecologia" && y.Adjudicated == true).Count() * 100.0 / Math.Max(emitidasEcologia, 1)).ToString("0.##");
                            //kpi.Target = 40;
                            //EvaluateValueBusinessPositiveBetter(kpi, emitidasEcologia, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Outros
                            //kpi = listKPI.First(x => x.Name == "DN20b");
                            //valueString = (listBusinessDevDataYear.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia" && y.Adjudicated == true).Count() * 100.0 / Math.Max(emitidasOutros, 1)).ToString("0.##");
                            //kpi.Target = 30;
                            //EvaluateValueBusinessPositiveBetter(kpi, emitidasOutros, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Todos
                            //kpi = listKPI.First(x => x.Description == DepartmentKPIs.BusinessDevelopment.RacioOfProposalsTotal.Item1);
                            //valueString = (listBusinessDevDataYear.Where(y => y.Adjudicated == true).Count() * 100.0 / Math.Max(emitidasTodos, 1)).ToString("0.##");
                            //kpi.Target = 50;
                            //EvaluateValueBusinessPositiveBetter(kpi, emitidasTodos, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //nº homens/dia adjudicados 
                            kpi = listKPI.First(x => x.Name == "DN19");
                            valueString = valueD.ToString("0,0.00");
                            kpi.Target = GetAccumulatedTarget(2300, month);
                            EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            Tools.SetKPIValue(month, kpi, valueString);
                            value = 0;
                            valueString = "";

                            ////Média da margem bruta de propostas emitidas Birdtrack
                            //kpi = listKPI.First(x => x.Name == "DN22");
                            //kpi.Target = 30;
                            //if (listBusinessDevDataYear.Where(y => y.Area == "Birdtrack").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevDataYear.Where(y => y.Area == "Birdtrack").Sum(z => z.Margin) / Math.Max(emitidasBT, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Média da margem bruta de propostas emitidas EIA e estudos
                            //kpi = listKPI.First(x => x.Name == "DN23");
                            //kpi.Target = 30;
                            //if (listBusinessDevDataYear.Where(y => y.Area == "EIA/Estudos").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevDataYear.Where(y => y.Area == "EIA/Estudos").Sum(z => z.Margin) / Math.Max(emitidasEIA, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Média da margem bruta de propostas emitidas Monitorização e ecologia
                            //kpi = listKPI.First(x => x.Name == "DN24");
                            //kpi.Target = 30;
                            //if (listBusinessDevDataYear.Where(y => y.Area == "Ecologia").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevDataYear.Where(y => y.Area == "Ecologia").Sum(z => z.Margin) / Math.Max(emitidasEcologia, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Média da margem bruta de propostas emitidas outros
                            //kpi = listKPI.First(x => x.Name == "DN25");
                            //kpi.Target = 30;
                            //if (listBusinessDevDataYear.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevDataYear.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia").Sum(z => z.Margin) / Math.Max(emitidasOutros, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Média da margem bruta de todas propostas emitidas 
                            //kpi = listKPI.First(x => x.Description == DepartmentKPIs.BusinessDevelopment.AvgMarginTotal.Item1);
                            //kpi.Target = 30;
                            //if (listBusinessDevDataYear.Count > 0)
                            //{
                            //    valueD = (listBusinessDevDataYear.Sum(z => z.Margin) / Math.Max(emitidasTodos, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            ////Nº prop. Cl. exitentes adjudicadas / Nº total de propostas cl. existentes
                            //kpi = listKPI.First(x => x.Name == "DN26");
                            //valueD = ((uniqueProposalsYear.Where(y => y.NewClient == false && y.Adjudicated == true).Count() * 100) / Math.Max(uniqueProposalsYear.Where(y => y.NewClient == false).Count(), 1));
                            //valueString = valueD.ToString("0.##");
                            //kpi.Target = 100;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //Nº de novos clientes
                            kpi = listKPI.First(x => x.Name == "DN21");
                            valueD = listBusinessDevDataYear.Where(y => y.NewClient == true).Select(x => x.Client).Distinct().Count();
                            kpi.Target = GetAccumulatedTarget(10, month);
                            EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            Tools.SetKPIValue(month, kpi, value);
                            value = 0;
                            valueString = "";

                            ////########################################################
                            //// ----------- Monthly Report -----------------------
                            ////########################################################
                            //kpi = listKPI.First(x => x.Name == "DN14b");
                            //valueD = monthlyUniqueProposals.Select(y => y.Proposal).ToList().Count;
                            //valueString = valueD.ToString();
                            //kpi.Target = 60 / 12;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN15b");
                            //kpi.Target = 3000000 / 12;
                            //valueString = monthlyTotalIssuedProposalsValue.ToString("0,0.00", CultureInfo.InvariantCulture);
                            //EvaluateValueBusinessPositiveBetter(kpi, monthlyTotalIssuedProposalsValue, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN16b");
                            //valueString = monthlyTotalApprovedProposalsValue.ToString("0,0.00", CultureInfo.InvariantCulture);
                            //kpi.Target = 570000 / 12;
                            //EvaluateValueBusinessPositiveBetter(kpi, monthlyTotalApprovedProposalsValue, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //// ratio entre valor valor adjudicado/emitido
                            //kpi = listKPI.First(x => x.Name == "_b1");
                            //valueString = (monthlyTotalApprovedProposalsValue / Math.Max(monthlyTotalIssuedProposalsValue, 1) * 100).ToString("0,0.00", CultureInfo.InvariantCulture);
                            //kpi.Target = 50;
                            //EvaluateValueBusinessPositiveBetter(kpi, monthlyTotalApprovedProposalsValue / monthlyTotalIssuedProposalsValue * 100, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN17b");
                            //valueD = (listBusinessDevData.Where(y => y.Area == "Birdtrack" && y.Adjudicated == true).Count() * 100.0 / Math.Max(monthlyEmitidasBT, 1));
                            //valueString = valueD.ToString("0.##");
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //kpi.Target = 40;
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN18b");
                            //valueD = (listBusinessDevData.Where(y => y.Area == "EIA/Estudos" && y.Adjudicated == true).Count() * 100.0 / Math.Max(monthlyEmitidasEIA, 1));
                            //valueString = valueD.ToString("0.##");
                            //kpi.Target = 30;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN19b");
                            //valueD = (listBusinessDevData.Where(y => y.Area == "Ecologia" && y.Adjudicated == true).Count() * 100.0 / Math.Max(monthlyEmitidasEcologia, 1));
                            //valueString = valueD.ToString("0.##");
                            //kpi.Target = 40;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN20b");
                            //valueD = (listBusinessDevData.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia" && y.Adjudicated == true).Count() * 100.0 / Math.Max(monthlyEmitidasOutros, 1));
                            //valueString = valueD.ToString("0.##");
                            //kpi.Target = 1;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "b2");
                            //valueD = (listBusinessDevData.Where(y => y.Adjudicated == true).Count() * 100.0 / Math.Max(monthlyEmitidasTodos, 1));
                            //valueString = valueD.ToString("0.##");
                            //kpi.Target = 50;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN21b");
                            //valueD = listBusinessDevData.Where(y => y.Adjudicated == true).Sum(x => x.ManDay);
                            //valueString = valueD.ToString("0,0.00");
                            //kpi.Target = 2300 / 12.0;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN22b");
                            //kpi.Target = 30;
                            //if (listBusinessDevData.Where(y => y.Area == "Birdtrack").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevData.Where(y => y.Area == "Birdtrack").Sum(z => z.Margin) / Math.Max(monthlyEmitidasBT, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD * 100, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN23b");
                            //kpi.Target = 30;
                            //if (listBusinessDevData.Where(y => y.Area == "EIA/Estudos").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevData.Where(y => y.Area == "EIA/Estudos").Sum(z => z.Margin) / Math.Max(monthlyEmitidasEIA, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD * 100, kpi.Target);

                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";


                            //kpi = listKPI.First(x => x.Name == "DN24b");
                            //kpi.Target = 30;
                            //if (listBusinessDevData.Where(y => y.Area == "Ecologia").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevData.Where(y => y.Area == "Ecologia").Sum(z => z.Margin) / Math.Max(monthlyEmitidasEcologia, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD * 100, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN25b");
                            //kpi.Target = 30;
                            //if (listBusinessDevData.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia").Count() > 0)
                            //{
                            //    valueD = (listBusinessDevData.Where(y => y.Area != "EIA/Estudos" && y.Area != "Birdtrack" && y.Area != "Ecologia").Sum(z => z.Margin) / Math.Max(monthlyEmitidasOutros, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD * 100, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, 0, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "b3");
                            //kpi.Target = 30;
                            //if (listBusinessDevData.Count > 0)
                            //{
                            //    valueD = (listBusinessDevData.Sum(z => z.Margin) / Math.Max(monthlyEmitidasTodos, 1));
                            //    valueString = valueD.ToString("0.##");
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD * 100, kpi.Target);
                            //}
                            //else
                            //{
                            //    valueString = "-";
                            //    EvaluateValueBusinessPositiveBetter(kpi, valueD * 100, kpi.Target);
                            //}
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN26b");
                            //valueD = ((monthlyUniqueProposals.Where(y => y.NewClient == false && y.Adjudicated == true).Count() * 100) / Math.Max(monthlyUniqueProposals.Where(y => y.NewClient == false).Count(), 1));
                            //valueString = valueD.ToString("0.##");
                            //kpi.Target = 100;
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";

                            //kpi = listKPI.First(x => x.Name == "DN27b");
                            //kpi.Target = 10 / 12.0;
                            //valueD = listBusinessDevData.Where(y => y.NewClient == true).Select(x => x.Client).Distinct().Count();
                            //valueString = valueD.ToString();
                            //EvaluateValueBusinessPositiveBetter(kpi, valueD, kpi.Target);
                            //Tools.SetKPIValue(month, kpi, valueString);
                            //value = 0;
                            //valueString = "";
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    });
                }
            }
            ExportKPIsToExcel(year, listKPI);
            return listKPI;
        }

        private static void EvaluateValueBusinessPositiveBetter(KPI kpi, double value, double target)
        {
            if (value > target)
            {
                kpi.Evaluation = "Above Target";
            }
            else if ((value > 0.75 * target) && (value < 0.95 * target))
            {
                kpi.Evaluation = "Below Target";
            }
            else if (value <= 0.8 * target)
            {
                kpi.Evaluation = "Critical Target";
            }
            else
            {
                kpi.Evaluation = "Normal";
            }
        }
        #endregion

        #region Private methods
        private string getDateTimeStringFromStringPT(string date)
        {
            string newDateString = date.ToLower().Replace("jan", "01")
                                                 .Replace("fev", "02")
                                                 .Replace("mar", "03")
                                                 .Replace("abr", "04")
                                                 .Replace("mai", "05")
                                                 .Replace("jun", "06")
                                                 .Replace("jul", "07")
                                                 .Replace("ago", "08")
                                                 .Replace("set", "09")
                                                 .Replace("out", "10")
                                                 .Replace("nov", "11")
                                                 .Replace("dez", "12");

            if (newDateString.Split('-')[2].Length == 2)
            {
                newDateString = newDateString.Insert(6, "20");
            }

            return newDateString;
        }

        private static void ExportKPIsToExcel(int year, List<KPI> listKPI)
        {
            string filePath = $"C:\\Temp\\KPIs_NegocioPropostas_{year}.xlsx";
            int ver = 1;
            while (File.Exists(filePath))
            {
                filePath = $"C:\\Temp\\KPIs_NegocioPropostas_{year}_{ver++}.xlsx";
            }
            var package = new ExcelPackage(new FileInfo(filePath));
            ExcelWorksheet TDevSheet = package.Workbook.Worksheets.Add("KPIs");
            int col = 1;
            int idx = 1;
            TDevSheet.Cells[idx, col++].Value = "Name";
            TDevSheet.Cells[idx, col++].Value = "Description";
            TDevSheet.Cells[idx, col++].Value = "Jan";
            TDevSheet.Cells[idx, col++].Value = "Fev";
            TDevSheet.Cells[idx, col++].Value = "Mar";
            TDevSheet.Cells[idx, col++].Value = "Abr";
            TDevSheet.Cells[idx, col++].Value = "Mai";
            TDevSheet.Cells[idx, col++].Value = "Jun";
            TDevSheet.Cells[idx, col++].Value = "Jul";
            TDevSheet.Cells[idx, col++].Value = "Ago";
            TDevSheet.Cells[idx, col++].Value = "Set";
            TDevSheet.Cells[idx, col++].Value = "Out";
            TDevSheet.Cells[idx, col++].Value = "Nov";
            TDevSheet.Cells[idx, col++].Value = "Dez";
            TDevSheet.Cells[idx, col++].Value = "Units";
            TDevSheet.Cells[idx, col++].Value = "Target";
            TDevSheet.Cells[idx, col++].Value = "Department";
            idx++;
            try
            {
                foreach (var elem in listKPI)
                {
                    col = 1;
                    TDevSheet.Cells[idx, col++].Value = elem.Name;
                    TDevSheet.Cells[idx, col++].Value = elem.Description;
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueJan == null || elem.ValueJan == string.Empty || elem.ValueJan == "-") ? 0 : Double.Parse(elem.ValueJan);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueFev == null || elem.ValueFev == string.Empty || elem.ValueFev == "-") ? 0 : Double.Parse(elem.ValueFev);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueMar == null || elem.ValueMar == string.Empty || elem.ValueMar == "-") ? 0 : Double.Parse(elem.ValueMar);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueAbr == null || elem.ValueAbr == string.Empty || elem.ValueAbr == "-") ? 0 : Double.Parse(elem.ValueAbr);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueMai == null || elem.ValueMai == string.Empty || elem.ValueMai == "-") ? 0 : Double.Parse(elem.ValueMai);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueJun == null || elem.ValueJun == string.Empty || elem.ValueJun == "-") ? 0 : Double.Parse(elem.ValueJun);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueJul == null || elem.ValueJul == string.Empty || elem.ValueJul == "-") ? 0 : Double.Parse(elem.ValueJul);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueAgo == null || elem.ValueAgo == string.Empty || elem.ValueAgo == "-") ? 0 : Double.Parse(elem.ValueAgo);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueSet == null || elem.ValueSet == string.Empty || elem.ValueSet == "-") ? 0 : Double.Parse(elem.ValueSet);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueOut == null || elem.ValueOut == string.Empty || elem.ValueOut == "-") ? 0 : Double.Parse(elem.ValueOut);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueNov == null || elem.ValueNov == string.Empty || elem.ValueNov == "-") ? 0 : Double.Parse(elem.ValueNov);
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueDez == null || elem.ValueDez == string.Empty || elem.ValueDez == "-") ? 0 : Double.Parse(elem.ValueDez);
                    TDevSheet.Cells[idx, col++].Value = elem.Units;
                    TDevSheet.Cells[idx, col++].Value = elem.Target;
                    TDevSheet.Cells[idx, col++].Value = elem.Department;
                    idx++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            package.Save();
        }
        #endregion
    }
}
