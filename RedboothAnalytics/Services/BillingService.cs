﻿using Analytics.Models;
using Analytics.People;
using Common.Logs;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Analytics.Services
{
    public class BillingService
    {
        #region Fields
        ExcelWorksheet billingSheet;
        List<BillingData> listBillingData;
        List<BillingData> listBillingDataProjects;
        ExcelPackage package;
        #endregion

        #region Constructor
        public BillingService()
        {
        }
        #endregion

        #region Public methods

        public async Task<bool> ImportExcel(string filePath)
        {
            bool ret = false;
            listBillingData = new List<BillingData>();
            listBillingDataProjects = new List<BillingData>();
            await Task.Run(() =>
            {
                try
                {
                    package = new ExcelPackage(new FileInfo(filePath));
                    billingSheet = package.Workbook.Worksheets[1];
                    ret = true;
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothService", "ImportExcelToDataBase", ex.Message, ex.StackTrace);
                }
            });

            return ret;
        }

        public async Task<List<KPI>> CalculateMetrics(int year)
        {
            List<KPI> listKPI = new List<KPI>();
            await Task.Run(() =>
            {
                try
                {

                    ParseBillingSheet(year);
                    listKPI = getKPPIs(year);

                }
                catch (Exception ex)
                {
                    ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "BillingService", "CalculateMetrics", ex.Message, ex.StackTrace);
                }
            });
            return listKPI;
        }
        #endregion

        #region Private Methods
        private static void ExportKPIsToExcel(string kpiName, int year, List<KPI> listKPI)
        {
            string filePath = $"C:\\Temp\\{kpiName}_{year}.xlsx";
            int ver = 1;
            while (File.Exists(filePath))
            {
                filePath = $"C:\\Temp\\{kpiName}_{year}_{ver++}.xlsx";
            }
            var package = new ExcelPackage(new FileInfo(filePath));
            ExcelWorksheet TDevSheet = package.Workbook.Worksheets.Add("KPIs");
            int col = 1;
            int idx = 1;
            TDevSheet.Cells[idx, col++].Value = "Name";
            //TDevSheet.Cells[idx, col++].Value = "Description";
            TDevSheet.Cells[idx, col++].Value = "Jan";
            TDevSheet.Cells[idx, col++].Value = "Fev";
            TDevSheet.Cells[idx, col++].Value = "Mar";
            TDevSheet.Cells[idx, col++].Value = "Abr";
            TDevSheet.Cells[idx, col++].Value = "Mai";
            TDevSheet.Cells[idx, col++].Value = "Jun";
            TDevSheet.Cells[idx, col++].Value = "Jul";
            TDevSheet.Cells[idx, col++].Value = "Ago";
            TDevSheet.Cells[idx, col++].Value = "Set";
            TDevSheet.Cells[idx, col++].Value = "Out";
            TDevSheet.Cells[idx, col++].Value = "Nov";
            TDevSheet.Cells[idx, col++].Value = "Dez";
            TDevSheet.Cells[idx, col++].Value = "Units";
            TDevSheet.Cells[idx, col++].Value = "Target";
            TDevSheet.Cells[idx, col++].Value = "Department";
            idx++;
            foreach (var elem in listKPI)
            {
                col = 1;

                TDevSheet.Cells[idx, col++].Value = elem.Name;

                //TDevSheet.Cells[idx, col++].Value = elem.Description;
                if (!(elem.ValueJan == null || elem.ValueJan == string.Empty) || elem.ValueJan == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueJan == null || elem.ValueJan == string.Empty) ? 0 : Double.Parse(elem.ValueJan);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueFev == null || elem.ValueFev == string.Empty) || elem.ValueFev == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueFev == null || elem.ValueFev == string.Empty) ? 0 : Double.Parse(elem.ValueFev);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueMar == null || elem.ValueMar == string.Empty) || elem.ValueMar == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueMar == null || elem.ValueMar == string.Empty) ? 0 : Double.Parse(elem.ValueMar);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueAbr == null || elem.ValueAbr == string.Empty) || elem.ValueAbr == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueAbr == null || elem.ValueAbr == string.Empty) ? 0 : Double.Parse(elem.ValueAbr);
                }
                if (!(elem.ValueMai == null || elem.ValueMai == string.Empty) || elem.ValueMai == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueMai == null || elem.ValueMai == string.Empty) ? 0 : Double.Parse(elem.ValueMai);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueJun == null || elem.ValueJun == string.Empty) || elem.ValueJun == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueJun == null || elem.ValueJun == string.Empty) ? 0 : Double.Parse(elem.ValueJun);
                }
                if (!(elem.ValueJul == null || elem.ValueJul == string.Empty) || elem.ValueJul == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueJul == null || elem.ValueJul == string.Empty) ? 0 : Double.Parse(elem.ValueJul);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }

                if (!(elem.ValueAgo == null || elem.ValueAgo == string.Empty) || elem.ValueAgo == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueAgo == null || elem.ValueAgo == string.Empty) ? 0 : Double.Parse(elem.ValueAgo);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueSet == null || elem.ValueSet == string.Empty) || elem.ValueSet == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueSet == null || elem.ValueSet == string.Empty) ? 0 : Double.Parse(elem.ValueSet);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueOut == null || elem.ValueOut == string.Empty) || elem.ValueOut == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueOut == null || elem.ValueOut == string.Empty) ? 0 : Double.Parse(elem.ValueOut);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueNov == null || elem.ValueNov == string.Empty) || elem.ValueNov == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueNov == null || elem.ValueNov == string.Empty) ? 0 : Double.Parse(elem.ValueNov);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                if (!(elem.ValueDez == null || elem.ValueDez == string.Empty) || elem.ValueDez == "0")
                {
                    TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                    TDevSheet.Cells[idx, col++].Value = (elem.ValueDez == null || elem.ValueDez == string.Empty) ? 0 : Double.Parse(elem.ValueDez);
                }
                else
                {
                    TDevSheet.Cells[idx, col++].Value = null;
                }
                TDevSheet.Cells[idx, col++].Value = "€";
                TDevSheet.Cells[idx, col].Style.Numberformat.Format = "€#,##0.00";
                TDevSheet.Cells[idx, col++].Value = elem.Target;
                TDevSheet.Cells[idx, col++].Value = elem.Department;
                idx++;
            }
            package.Save();
        }

        private void ParseBillingSheet(int year)
        {
            if (package != null)
            {
                listBillingData = new List<BillingData>();
                listBillingDataProjects = new List<BillingData>();
                bool isStarted = false;
                int sheetCnt = 6;
                while (true)
                {
                    try
                    {
                        if (sheetCnt == 165) // sheet limit (no more sheet)
                        {
                            break;
                        }
                        billingSheet = package.Workbook.Worksheets[sheetCnt];
                        if (billingSheet != null)// Valid Sheet
                        {
                            int cnt = 13;

                            if (billingSheet.Cells[cnt, 1].Text.ToLower().Contains("total"))
                            {
                                cnt++;
                                while (true)
                                {

                                    if (isStarted && billingSheet.Cells[cnt, 1].Text != null && billingSheet.Cells[cnt, 1].Text.Length == 0)// End of valid values
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        isStarted = true;
                                        //string[] yearString = billingSheet.Cells[cnt, 1].Text.Split(' ');
                                        //if (!billingSheet.Cells[cnt, 1].Text.ToString().ToLower().Contains("total"))
                                        //&& yearString.Count() == 2 && "20" + yearString[1] == year.ToString())//se não for um sub total e for o ano escolhido, calcular 

                                        int sheetYear = 0;
                                        string[] tempSPlit = billingSheet.Cells[cnt, 1].Text.Split(' ');
                                        if (tempSPlit.Length == 2)
                                        {
                                            Int32.TryParse(tempSPlit[1], out sheetYear);
                                            sheetYear += 2000;
                                        }
                                        if (sheetYear == year)
                                        {
                                            BillingData billingData = new BillingData();
                                            BillingData billingDataproject = new BillingData();
                                            billingData.Sheet = billingSheet.Name;

                                            billingData.Date = getDate(billingSheet.Cells[cnt, 1].Text.ToString()).Value;

                                            double baseLine, Prediction, Billed;

                                            double.TryParse(billingSheet.Cells[cnt, 2].Text.Replace("€", "").Trim(), out Prediction);
                                            double.TryParse(billingSheet.Cells[cnt, 4].Text.Replace("€", "").Trim(), out Billed);
                                            double.TryParse(billingSheet.Cells[cnt, 6].Text.Replace("€", "").Trim(), out baseLine);

                                            billingData.Baseline = baseLine;
                                            billingData.Prediction = Prediction;
                                            billingData.Billed = Billed;

                                            billingDataproject = clonebillingData(billingData);


                                            listBillingDataProjects.Add(billingDataproject);

                                            string json = JsonConvert.SerializeObject(listBillingDataProjects, Formatting.Indented);

                                            if (listBillingData.Where(x => x.Date == billingData.Date) != null
                                                && listBillingData.Where(x => x.Date == billingData.Date).ToList().Count > 0)
                                            {
                                                listBillingData.First(x => x.Date == billingData.Date).Baseline += billingData.Baseline;
                                                listBillingData.First(x => x.Date == billingData.Date).Prediction += billingData.Prediction;
                                                listBillingData.First(x => x.Date == billingData.Date).Billed += billingData.Billed;
                                            }
                                            else
                                            {
                                                listBillingData.Add(billingData);
                                            }
                                        }
                                    }
                                    cnt++;
                                }
                            }
                            sheetCnt++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "BillingcService", "ParseBillingSheet", ex.Message, ex.StackTrace);
                    }
                }
                //Create KPIs
            }
            else
            {
                MessageBox.Show("Load Excel first!");
            }
        }

        private List<KPI> getKPPIs(int year)
        {
            List<KPI> listKPI = new List<KPI>();

            try
            {
                KPI kpiPrediction = new KPI();
                kpiPrediction.Name = "Prediction " + year.ToString();
                kpiPrediction.ValueJan = listBillingData.Where(x => x.Date.Month == 1).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueFev = listBillingData.Where(x => x.Date.Month == 2).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueMar = listBillingData.Where(x => x.Date.Month == 3).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueAbr = listBillingData.Where(x => x.Date.Month == 4).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueMai = listBillingData.Where(x => x.Date.Month == 5).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueJun = listBillingData.Where(x => x.Date.Month == 6).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueJul = listBillingData.Where(x => x.Date.Month == 7).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueAgo = listBillingData.Where(x => x.Date.Month == 8).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueSet = listBillingData.Where(x => x.Date.Month == 9).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueOut = listBillingData.Where(x => x.Date.Month == 10).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueNov = listBillingData.Where(x => x.Date.Month == 11).Sum(y => y.Prediction).ToString();
                kpiPrediction.ValueDez = listBillingData.Where(x => x.Date.Month == 12).Sum(y => y.Prediction).ToString();
                kpiPrediction.Target = listBillingData.Sum(x => x.Prediction);

                KPI kpiPredictionAcc = new KPI();
                kpiPredictionAcc.Name = "Prediction Acc " + year.ToString();
                kpiPredictionAcc.ValueJan = listBillingData.Where(x => x.Date.Month == 1).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueFev = listBillingData.Where(x => x.Date.Month <= 2).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueMar = listBillingData.Where(x => x.Date.Month <= 3).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueAbr = listBillingData.Where(x => x.Date.Month <= 4).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueMai = listBillingData.Where(x => x.Date.Month <= 5).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueJun = listBillingData.Where(x => x.Date.Month <= 6).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueJul = listBillingData.Where(x => x.Date.Month <= 7).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueAgo = listBillingData.Where(x => x.Date.Month <= 8).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueSet = listBillingData.Where(x => x.Date.Month <= 9).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueOut = listBillingData.Where(x => x.Date.Month <= 10).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueNov = listBillingData.Where(x => x.Date.Month <= 11).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.ValueDez = listBillingData.Where(x => x.Date.Month <= 12).Sum(y => y.Prediction).ToString();
                kpiPredictionAcc.Target = listBillingData.Sum(x => x.Prediction);

                KPI kpiBilled = new KPI();
                kpiBilled.Name = "Billed " + year.ToString();
                kpiBilled.ValueJan = listBillingData.Where(x => x.Date.Month == 1).Sum(y => y.Billed).ToString();
                kpiBilled.ValueFev = listBillingData.Where(x => x.Date.Month == 2).Sum(y => y.Billed).ToString();
                kpiBilled.ValueMar = listBillingData.Where(x => x.Date.Month == 3).Sum(y => y.Billed).ToString();
                kpiBilled.ValueAbr = listBillingData.Where(x => x.Date.Month == 4).Sum(y => y.Billed).ToString();
                kpiBilled.ValueMai = listBillingData.Where(x => x.Date.Month == 5).Sum(y => y.Billed).ToString();
                kpiBilled.ValueJun = listBillingData.Where(x => x.Date.Month == 6).Sum(y => y.Billed).ToString();
                kpiBilled.ValueJul = listBillingData.Where(x => x.Date.Month == 7).Sum(y => y.Billed).ToString();
                kpiBilled.ValueAgo = listBillingData.Where(x => x.Date.Month == 8).Sum(y => y.Billed).ToString();
                kpiBilled.ValueSet = listBillingData.Where(x => x.Date.Month == 9).Sum(y => y.Billed).ToString();
                kpiBilled.ValueOut = listBillingData.Where(x => x.Date.Month == 10).Sum(y => y.Billed).ToString();
                kpiBilled.ValueNov = listBillingData.Where(x => x.Date.Month == 11).Sum(y => y.Billed).ToString();
                kpiBilled.ValueDez = listBillingData.Where(x => x.Date.Month == 12).Sum(y => y.Billed).ToString();
                kpiBilled.Target = listBillingData.Sum(x => x.Billed);

                KPI kpiBilledAcc = new KPI();
                kpiBilledAcc.Name = "Billed Acc " + year.ToString();
                kpiBilledAcc.ValueJan = listBillingData.Where(x => x.Date.Month == 1).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueFev = listBillingData.Where(x => x.Date.Month <= 2).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueMar = listBillingData.Where(x => x.Date.Month <= 3).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueAbr = listBillingData.Where(x => x.Date.Month <= 4).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueMai = listBillingData.Where(x => x.Date.Month <= 5).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueJun = listBillingData.Where(x => x.Date.Month <= 6).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueJul = listBillingData.Where(x => x.Date.Month <= 7).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueAgo = listBillingData.Where(x => x.Date.Month <= 8).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueSet = listBillingData.Where(x => x.Date.Month <= 9).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueOut = listBillingData.Where(x => x.Date.Month <= 10).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueNov = listBillingData.Where(x => x.Date.Month <= 11).Sum(y => y.Billed).ToString();
                kpiBilledAcc.ValueDez = listBillingData.Where(x => x.Date.Month <= 12).Sum(y => y.Billed).ToString();
                kpiBilledAcc.Target = listBillingData.Sum(x => x.Billed);

                KPI kpiBaseline = new KPI();
                kpiBaseline.Name = "Baseline " + year.ToString();
                kpiBaseline.ValueJan = listBillingData.Where(x => x.Date.Month == 1).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueFev = listBillingData.Where(x => x.Date.Month == 2).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueMar = listBillingData.Where(x => x.Date.Month == 3).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueAbr = listBillingData.Where(x => x.Date.Month == 4).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueMai = listBillingData.Where(x => x.Date.Month == 5).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueJun = listBillingData.Where(x => x.Date.Month == 6).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueJul = listBillingData.Where(x => x.Date.Month == 7).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueAgo = listBillingData.Where(x => x.Date.Month == 8).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueSet = listBillingData.Where(x => x.Date.Month == 9).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueOut = listBillingData.Where(x => x.Date.Month == 10).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueNov = listBillingData.Where(x => x.Date.Month == 11).Sum(y => y.Baseline).ToString();
                kpiBaseline.ValueDez = listBillingData.Where(x => x.Date.Month == 12).Sum(y => y.Baseline).ToString();
                kpiBaseline.Target = listBillingData.Sum(x => x.Baseline);

                KPI kpiBaselineAcc = new KPI();
                kpiBaselineAcc.Name = "Baseline Acc" + year.ToString();
                kpiBaselineAcc.ValueJan = listBillingData.Where(x => x.Date.Month <= 1).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueFev = listBillingData.Where(x => x.Date.Month <= 2).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueMar = listBillingData.Where(x => x.Date.Month <= 3).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueAbr = listBillingData.Where(x => x.Date.Month <= 4).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueMai = listBillingData.Where(x => x.Date.Month <= 5).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueJun = listBillingData.Where(x => x.Date.Month <= 6).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueJul = listBillingData.Where(x => x.Date.Month <= 7).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueAgo = listBillingData.Where(x => x.Date.Month <= 8).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueSet = listBillingData.Where(x => x.Date.Month <= 9).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueOut = listBillingData.Where(x => x.Date.Month <= 10).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueNov = listBillingData.Where(x => x.Date.Month <= 11).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.ValueDez = listBillingData.Where(x => x.Date.Month <= 12).Sum(y => y.Baseline).ToString();
                kpiBaselineAcc.Target = listBillingData.Sum(x => x.Baseline);

                listKPI.Add(kpiPrediction);
                listKPI.Add(kpiPredictionAcc);

                listKPI.Add(kpiBilled);
                listKPI.Add(kpiBilledAcc);

                listKPI.Add(kpiBaseline);
                listKPI.Add(kpiBaselineAcc);

                KPI spaceKPI = new KPI();
                listKPI.Add(spaceKPI);

                List<string> projects = listBillingDataProjects.Select(y => y.Sheet).Distinct().ToList();

                //por projecto
                KPI tempKPI = new KPI() { Name = "Por projecto" };
                listKPI.Add(tempKPI);
                foreach (string project in projects)
                {
                    KPI kpiProjectPrediction = new KPI();
                    kpiProjectPrediction.ValueJan = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 1).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueFev = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 2).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueMar = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 3).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueAbr = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 4).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueMai = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 5).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueJun = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 6).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueJul = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 7).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueAgo = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 8).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueSet = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 9).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueOut = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 10).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueNov = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 11).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueDez = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 12).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.Name = "Prediction " + project;
                    kpiProjectPrediction.Target = listBillingDataProjects.Where(x => x.Sheet == project).Sum(x => x.Prediction);

                    KPI kpiProjectBilled = new KPI();
                    kpiProjectBilled.ValueJan = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 1).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueFev = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 2).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueMar = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 3).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueAbr = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 4).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueMai = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 5).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueJun = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 6).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueJul = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 7).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueAgo = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 8).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueSet = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 9).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueOut = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 10).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueNov = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 11).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueDez = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 12).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.Name = "Billed " + project;
                    kpiProjectBilled.Target = listBillingDataProjects.Where(x => x.Sheet == project).Sum(x => x.Billed);

                    KPI kpiProjectBaseline = new KPI();
                    kpiProjectBaseline.ValueJan = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 1).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueFev = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 2).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueMar = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 3).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueAbr = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 4).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueMai = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 5).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueJun = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 6).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueJul = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 7).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueAgo = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 8).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueSet = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 9).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueOut = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 10).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueNov = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 11).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueDez = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month == 12).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.Name = "Baseline " + project;
                    kpiProjectBaseline.Target = listBillingDataProjects.Where(x => x.Sheet == project).Sum(x => x.Baseline);

                    if (hasAtLeastOneValue(kpiProjectBaseline) || hasAtLeastOneValue(kpiProjectBilled) || hasAtLeastOneValue(kpiProjectPrediction))
                    {
                        listKPI.Add(kpiProjectPrediction);
                        listKPI.Add(kpiProjectBilled);
                        // listKPI.Add(kpiProjectBaseline);

                        spaceKPI = new KPI();
                        listKPI.Add(spaceKPI);
                    }
                }

                //acumulado
                tempKPI = new KPI() { Name = "Acumulado" };
                listKPI.Add(tempKPI);
                foreach (string project in projects)
                {
                    KPI kpiProjectPrediction = new KPI();
                    kpiProjectPrediction.ValueJan = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 1).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueFev = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 2).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueMar = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 3).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueAbr = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 4).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueMai = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 5).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueJun = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 6).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueJul = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 7).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueAgo = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 8).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueSet = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 9).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueOut = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 10).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueNov = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 11).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.ValueDez = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 12).Sum(y => y.Prediction).ToString();
                    kpiProjectPrediction.Name = "Prediction " + project;
                    kpiProjectPrediction.Target = listBillingDataProjects.Where(x => x.Sheet == project).Sum(x => x.Prediction);

                    KPI kpiProjectBilled = new KPI();
                    kpiProjectBilled.ValueJan = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 1).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueFev = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 2).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueMar = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 3).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueAbr = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 4).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueMai = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 5).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueJun = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 6).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueJul = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 7).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueAgo = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 8).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueSet = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 9).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueOut = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 10).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueNov = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 11).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.ValueDez = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 12).Sum(y => y.Billed).ToString();
                    kpiProjectBilled.Name = "Billed " + project;
                    kpiProjectBilled.Target = listBillingDataProjects.Where(x => x.Sheet == project).Sum(x => x.Billed);

                    KPI kpiProjectBaseline = new KPI();
                    kpiProjectBaseline.ValueJan = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 1).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueFev = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 2).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueMar = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 3).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueAbr = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 4).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueMai = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 5).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueJun = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 6).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueJul = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 7).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueAgo = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 8).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueSet = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 9).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueOut = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 10).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueNov = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 11).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.ValueDez = listBillingDataProjects.Where(x => x.Sheet == project && x.Date.Month <= 12).Sum(y => y.Baseline).ToString();
                    kpiProjectBaseline.Name = "Baseline " + project;
                    kpiProjectBaseline.Target = listBillingDataProjects.Where(x => x.Sheet == project).Sum(x => x.Baseline);

                    if (kpiProjectBaseline.ValueDez != "0" || kpiProjectBilled.ValueDez != "0" || kpiProjectPrediction.ValueDez != "0")
                    {
                        //listKPI.Add(kpiProjectPrediction);
                        //listKPI.Add(kpiProjectBilled);
                        //listKPI.Add(kpiProjectBaseline);

                        //spaceKPI = new KPI();
                        //listKPI.Add(spaceKPI);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "BillingService", "getKPPIs", ex.Message, ex.StackTrace);
            }
            ExportKPIsToExcel("BillingReport", year, listKPI);
            return listKPI;
        }
        #endregion

        #region Private Methods
        private DateTime? getDate(string dateString)
        {
            if (string.IsNullOrEmpty(dateString))
            {
                return null;
            }

            DateTime dt = new DateTime();

            string[] splits = dateString.Split(' ');

            if (splits == null || splits.Count() != 2)
            {
                return null;
            }

            dt = new DateTime(2000 + Int32.Parse(splits[1]), getMonth(splits[0]), 1);
            return dt;
        }
        private int getMonth(string monthString)
        {
            int month = 0;

            switch (monthString.ToLower())
            {
                case "janeiro":
                    month = 1;
                    break;
                case "fevereiro":
                    month = 2;
                    break;
                case "março":
                    month = 3;
                    break;
                case "abril":
                    month = 4;
                    break;
                case "maio":
                    month = 5;
                    break;
                case "junho":
                    month = 6;
                    break;
                case "julho":
                    month = 7;
                    break;
                case "agosto":
                    month = 8;
                    break;
                case "setembro":
                    month = 9;
                    break;
                case "outubro":
                    month = 10;
                    break;
                case "novembro":
                    month = 11;
                    break;
                case "dezembro":
                    month = 12;
                    break;
            }

            return month;
        }
        private bool hasAtLeastOneValue(KPI kpi)
        {
            if (kpi == null)
            {
                return false;
            }

            bool ret = false;

            if (kpi.ValueJan != "0"
                || kpi.ValueFev != "0"
                || kpi.ValueMar != "0"
                || kpi.ValueAbr != "0"
                || kpi.ValueMai != "0"
                || kpi.ValueJun != "0"
                || kpi.ValueJul != "0"
                || kpi.ValueAgo != "0"
                || kpi.ValueSet != "0"
                || kpi.ValueOut != "0"
                || kpi.ValueNov != "0"
                || kpi.ValueDez != "0")
            {
                ret = true;
            }

            return ret;

        }
        private BillingData clonebillingData(BillingData billingData)
        {
            if (billingData == null)
            {
                return null;
            }

            BillingData clone = new BillingData();

            clone.Baseline = billingData.Baseline;
            clone.Date = billingData.Date;
            clone.Prediction = billingData.Prediction;
            clone.Billed = billingData.Billed;
            clone.Sheet = billingData.Sheet;

            return clone;
        }
        #endregion
    }
}
