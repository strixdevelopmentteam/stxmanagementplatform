﻿using ManagementPlatform.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementPlatform.Locators
{
    public class ViewModelLocator
    {
        #region Fields
        private static ViewModelLocator instance = null;

        private static RedboothViewModel _RedboothViewModel = null;
        private static BusinessDevViewModel _BusinessDevViewModel = null;
        private static BillingViewModel _BillingViewModel = null;
        private static ManagementPlatformViewModel _ManagementPlatformViewModel = null;

        #endregion

        #region Static Properties

        public static ViewModelLocator Instance
        {
            get
            {
                if (instance == null)
                    instance = new ViewModelLocator();
                return instance;
            }
        }
        #endregion

        #region Constructor
        public ViewModelLocator()
        {
            if (instance != null)
            {
                throw new Exception("ViewModelLocator cannot have multiple instances!");
            }
        }
        #endregion

        #region VMs

        public RedboothViewModel RedboothViewModel
        {
            get
            {
                if (_RedboothViewModel == null)
                    _RedboothViewModel = new RedboothViewModel();
                return _RedboothViewModel;
            }
        }
        public BusinessDevViewModel BusinessDevViewModel
        {
            get
            {
                if (_BusinessDevViewModel == null)
                    _BusinessDevViewModel = new BusinessDevViewModel();
                return _BusinessDevViewModel;
            }
        }
        public BillingViewModel BillingViewModel
        {
            get
            {
                if (_BillingViewModel == null)
                    _BillingViewModel = new BillingViewModel();
                return _BillingViewModel;
            }
        }

        
        public ManagementPlatformViewModel ManagementPlatformViewModel
        {
            get
            {
                if (_ManagementPlatformViewModel == null)
                    _ManagementPlatformViewModel = new ManagementPlatformViewModel();
                return _ManagementPlatformViewModel;
            }
        }

        #endregion
    }
}
