﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ManagementPlatform.Controls.BusySpinner
{
    /// <summary>
    /// Interaction logic for BusyControl.xaml
    /// </summary>
    public partial class BusyControl : UserControl
    {
        #region Fields
        Storyboard sb;
        int busyCount = 0;
        string message = string.Empty;
        #endregion

        #region Properties
        public int BusyCount
        {
            get
            {
                return busyCount;
            }
            private set
            {
                if (busyCount != value)
                {
                    busyCount = value;
                    StartStopWait();
                }
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
            private set
            {
                if (message != value)
                {
                    message = value;
                    messageBlock.Text = value;
                }
            }
        }
        #endregion

        #region Constructor
        public BusyControl()
        {
            InitializeComponent();

            sb = waitSpinner.FindResource("BusyStoryboard") as Storyboard;
            Storyboard.SetTarget(sb, waitSpinner);
        }
        #endregion

        #region Public Methods
        public void Start(string message = "")
        {
            BusyCount++;
            Message = message;
        }
        public void Stop()
        {
            BusyCount--;
        }
        public void Reset()
        {
            BusyCount = 0;
        }
        #endregion

        #region Private Methods
        private void StartStopWait()
        {
            if (busyCount <= 0)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    gridSpinner.Visibility = Visibility.Collapsed;
                    sb.Stop();
                    this.Visibility = Visibility.Collapsed;
                }));
            }
            else
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    gridSpinner.Visibility = Visibility.Visible;
                    sb.Begin();
                    this.Visibility = Visibility.Visible;
                }));
            }
        }
        #endregion

        #region Events
        private void GridSpinner_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Do nothing
        }
        #endregion
    }
}
