﻿using ManagementPlatform.Controls.BusySpinner;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ManagementPlatform.Tools
{
    public class UIElementLocator
    {
        #region Fields
        private Window window = null;
        private BusyControl busyControl = null;
        private static UIElementLocator instance;
        Dictionary<string, UIElement> uIElmentsDictionary = new Dictionary<string, UIElement>();
        Dictionary<string, Grid> containerGridDictionary = new Dictionary<string, Grid>();
        #endregion

        #region Static Properties
        /// <summary>
        /// Current instance page
        /// </summary>
        public Window Window
        {
            get { return window; }
            set
            {
                if (window == null)
                    window = value;
                else
                    throw new InvalidOperationException("Window is already set.");
            }
        }

        public BusyControl BusyControl
        {
            get
            {
                if (busyControl == null && UIElementLocator.Instance.Window != null)
                {
                    busyControl = UIElementLocator.Instance.Window.FindName("busyControl") as BusyControl;
                }
                return busyControl;
            }
        }
        /// <summary>
        /// Static instance
        /// </summary>
        public static UIElementLocator Instance
        {
            get
            {
                if (instance == null)
                    instance = new UIElementLocator();
                return instance;
            }
        }
        #endregion

        #region Constructor
        private UIElementLocator()
        {
        }
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Get the first UIElement of the grid in the instance page
        /// </summary>
        /// <param name="gridName"></param>
        /// <returns></returns>
        public static UIElement GetElementByContainerGridName(string gridName)
        {
            UIElement uIElement = null;

            Grid containerGrid = Instance.Window.FindName(gridName) as Grid;

            if (containerGrid.Children.Count > 0)
            {
                uIElement = containerGrid.Children[0];
            }
            return uIElement;
        }
        /// <summary>
        /// Get the grid in the instance page
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static UIElement GetContainerGridByName(string name)
        {
            Grid containerGrid = Instance.Window.FindName(name) as Grid;

            return containerGrid;
        }
        /// <summary>
        /// Get the first UIElement in a Grid belonging to the given page
        /// </summary>
        /// <param name="control"></param>
        /// <param name="containerGridName"></param>
        /// <returns></returns>
        public static UIElement GetElementByContainerGridNameFromControl(Control control, string containerGridName)
        {
            UIElement uIElement = null;
            uIElement = control.FindName(containerGridName) as UIElement;
            return uIElement;
        }
        #endregion
    }
}