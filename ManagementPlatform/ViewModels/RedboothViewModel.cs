﻿using Common.Base;
using Common.Commands;
using Common.Logs;
using ManagementPlatform.Tools;
using Analytics;
using RedboothDatabase;
using Analytics.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Analytics.Services;

namespace ManagementPlatform.ViewModels
{
    public class RedboothViewModel : ViewModelBase
    {
        #region Fields
        RedboothService redboothService;
        #endregion

        #region Properties
        private string filePath = "";
        public string FilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                if (value != filePath)
                {
                    filePath = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private List<ImportMetadata> listMetadata = new List<ImportMetadata>();
        public List<ImportMetadata> ListMetadata
        {
            get
            {
                return listMetadata;
            }
            set
            {
                if (value != listMetadata)
                {
                    listMetadata = value;
                    NotifyPropertyChanged();
                }
            }
        }
        //private ImportMetadata selectedMetadata;
        //public ImportMetadata SelectedMetadata
        //{
        //    get
        //    {
        //        return selectedMetadata;
        //    }
        //    set
        //    {
        //        if (value != selectedMetadata)
        //        {
        //            selectedMetadata = value;
        //            NotifyPropertyChanged();
        //        }
        //    }
        //}
        private List<KPI> listKPI = new List<KPI>();
        public List<KPI> ListKPI
        {
            get
            {
                return listKPI;
            }
            set
            {
                if (value != listKPI)
                {
                    listKPI = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool sendEmailEnable;
        public bool SendEmailEnable
        {
            get
            {
                return sendEmailEnable;
            }
            set
            {
                if (value != sendEmailEnable)
                {
                    sendEmailEnable = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private List<TimeReport> timeReport;
        public List<TimeReport> TimeReport
        {
            get
            {
                return timeReport;
            }
            set
            {
                if (value != timeReport)
                {
                    timeReport = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Commands
        public SimpleCommand LoadRedboothHoursCommand { get; set; }
        //public SimpleCommand LoadMetadatasCommand { get; set; }
        public SimpleCommand CalculateMetricsCommand { get; set; }
        public SimpleCommand CalculateWorkloadReportCommand { get; set; }
        #endregion

        #region Constructor
        public RedboothViewModel()
        {
            redboothService = new RedboothService();

            LoadRedboothHoursCommand = new SimpleCommand
            {
                ExecuteDelegate = x => LoadRedboothHours(x)
            };
            //LoadMetadatasCommand = new SimpleCommand
            //{
            //    ExecuteDelegate = x => LoadMetadatas(x)
            //};
            CalculateMetricsCommand = new SimpleCommand
            {
                ExecuteDelegate = x => CalculateMetrics(x)
            };
            CalculateWorkloadReportCommand = new SimpleCommand
            {
                ExecuteDelegate = x => CalculateWorkloadReport(x)
            };
        }
        #endregion

        #region PrivateMethods
        private async void LoadRedboothHours(object obj)
        {
            UIElementLocator.Instance.BusyControl.Start("Working...");
            try
            {
                bool ret = true;

                System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog
                {
                    InitialDirectory = Directory.GetCurrentDirectory(),
                    Title = "Browse Excel Files",

                    CheckFileExists = true,
                    CheckPathExists = true,
                    Multiselect = true,
                    DefaultExt = "xlsx",
                    Filter = "xlsx files (*.xlsx)|*.xlsx",
                    FilterIndex = 2,
                    RestoreDirectory = true,

                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    await Task.Run(() =>
                    {
                        for (int i = 0; i < openFileDialog.FileNames.Count(); i++)
                        {

                            //FilePath = openFileDialog.FileName;
                            FilePath = openFileDialog.FileNames[i];
                            if (i != 0)
                            {
                                ret &= redboothService.ImportExcelToDataBase(FilePath).Result;
                            }
                            else
                            {
                                ret &= redboothService.ImportExcelToDataBase(FilePath, true).Result;
                            }
                        }
                    });
                }

                if (!ret)
                {
                    MessageBox.Show("Error! \nView Log!");
                    FilePath = "";
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothViewModel", "LoadRedboothHours", ex.Message, ex.StackTrace);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                UIElementLocator.Instance.BusyControl.Stop();
            }
        }

        private async void CalculateMetrics(object obj)
        {
            UIElementLocator.Instance.BusyControl.Start("Working...");
            try
            {
                await Task.Run(() =>
                {
                    Tuple<DateTime, DateTime> dtRange = redboothService.GetTimeWindowForImportedData();

                    List<KPI> tempList = new List<KPI>();
                    tempList.AddRange(redboothService.CalculateMetricsBioInterns(dtRange.Item1).Result);
                    tempList.AddRange(redboothService.CalculateMetricsBioExterns(dtRange.Item1).Result);
                    tempList.AddRange(redboothService.CalculateMetricsBDev(dtRange.Item1).Result);
                    tempList.AddRange(redboothService.CalculateMetricsTDev(dtRange.Item1).Result);
                    ListKPI = new List<KPI>(tempList);
                });
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothViewModel", "CalculateMetrics", ex.Message, ex.StackTrace);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                UIElementLocator.Instance.BusyControl.Stop();
            }
        }

        private async void CalculateWorkloadReport(object obj)
        {
#if !DEBUG
            return;
#else
                UIElementLocator.Instance.BusyControl.Start("Working...");
                try
                {
                    await Task.Run(() =>
                    {
                        Tuple<DateTime, DateTime> dtRange = redboothService.GetTimeWindowForImportedData();

                        //Aqui quero carregar os dados de um único mês
                        //OpenFileDialog e "redboothService.ImportExcelToDataBase(openFileDialog.FileName).Result"
                        TimeReport = redboothService.CalculateWorkloadReport(dtRange.Item1, dtRange.Item2, sendEmailEnable).Result;
                    });
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothViewModel", "CalculateMetrics", ex.Message, ex.StackTrace);
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    UIElementLocator.Instance.BusyControl.Stop();
                }
#endif
        }
        #endregion
    }
}
