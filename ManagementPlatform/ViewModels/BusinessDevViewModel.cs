﻿using Common.Base;
using Common.Commands;
using Common.Logs;
using ManagementPlatform.Tools;
using Analytics;
using RedboothDatabase;
using Analytics.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Analytics.Services;

namespace ManagementPlatform.ViewModels
{
    public class BusinessDevViewModel : ViewModelBase
    {
        #region Fields
        BusinessDevService businessDevService;
        #endregion

        #region Properties
        private List<KPI> listKPI = new List<KPI>();
        public List<KPI> ListKPI
        {
            get
            {
                return listKPI;
            }
            set
            {
                if (value != listKPI)
                {
                    listKPI = value;
                    NotifyPropertyChanged();
                }
            }
        }
        //private int month;
        //public int Month
        //{
        //    get
        //    {
        //        return month;
        //    }
        //    set
        //    {
        //        if (value != month)
        //        {
        //            month = value;
        //            NotifyPropertyChanged();
        //        }
        //    }
        //}
        private int year;
        public int Year
        {
            get
            {
                return year;
            }
            set
            {
                if (value != year)
                {
                    year = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Commands
        public SimpleCommand LoadXlsxCommand { get; set; }
        public SimpleCommand CalculateMetricsCommand { get; set; }
        #endregion

        #region Constructor
        public BusinessDevViewModel()
        {
            businessDevService = new BusinessDevService();

            LoadXlsxCommand = new SimpleCommand
            {
                ExecuteDelegate = x => LoadXlsx(x)
            };
            CalculateMetricsCommand = new SimpleCommand
            {
                ExecuteDelegate = x => CalculateMetrics(x)
            };

            Year = DateTime.Now.Year;
            //Month = DateTime.Now.Month;
        }
        #endregion

        #region PrivateMethods
        private async void LoadXlsx(object obj)
        {
            UIElementLocator.Instance.BusyControl.Start("Working...");
            try
            {
                bool ret = false;

                System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog
                {
                    InitialDirectory = Directory.GetCurrentDirectory(),
                    Title = "Browse Excel Files",

                    CheckFileExists = true,
                    CheckPathExists = true,

                    DefaultExt = "xlsx",
                    Filter = "xlsx files (*.xlsx)|*.xlsx",
                    FilterIndex = 2,
                    RestoreDirectory = true,

                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    await Task.Run(() =>
                    {
                        ret = businessDevService.ImportExcel(openFileDialog.FileName).Result;
                    });
                }

                if (!ret)
                {
                    MessageBox.Show("Error! \nView Log!");
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothViewModel", "LoadRedboothHours", ex.Message, ex.StackTrace);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                UIElementLocator.Instance.BusyControl.Stop();
            }
        }

        private async void CalculateMetrics(object obj)
        {
            UIElementLocator.Instance.BusyControl.Start("Working...");
            try
            {
                await Task.Run(() =>
                {
                    ListKPI = businessDevService.CalculateMetrics(year).Result;
                });
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToFile(System.Diagnostics.EventLogEntryType.Error, "RedboothViewModel", "CalculateMetrics", ex.Message, ex.StackTrace);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                UIElementLocator.Instance.BusyControl.Stop();
            }
        }
        #endregion
    }
}
