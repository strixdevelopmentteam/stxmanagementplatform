﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementPlatform.ViewModels
{
    public class ManagementPlatformViewModel : ViewModelBase
    {
        #region Window properties
        private string windowTitle = "Strix Management Platform";
        public string WindowTitle
        {
            get
            {
                return windowTitle;
            }
            set
            {
                if (value != windowTitle)
                {
                    windowTitle = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string iconSource = "/ManagementPlatform;component/Images/Icons/Owl.png";
        public string IconSource
        {
            get
            {
                return iconSource;
            }
            set
            {
                if (value != iconSource)
                {
                    iconSource = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region Constructor
        public ManagementPlatformViewModel()
        {
        }
        #endregion  
    }
}
